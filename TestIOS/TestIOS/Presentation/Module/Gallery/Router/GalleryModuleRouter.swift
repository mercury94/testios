//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit
import Swinject

class GalleryModuleRouter: GalleryWireframeProtocol {
    var container: Container? = nil
    var viewController: UIViewController? = nil

    func navigateToImageViewer(images: [String], selectedImageIndex: Int = 0) {
        var imageViewerViewController = container?.resolve(ImageViewerViewProtocol.self)
        imageViewerViewController?.presenter?.images = images
        imageViewerViewController?.presenter?.firstShowImageIndex = selectedImageIndex
        viewController?.navigationController?.pushViewController(imageViewerViewController as! UIViewController, animated: true)
    }
}
