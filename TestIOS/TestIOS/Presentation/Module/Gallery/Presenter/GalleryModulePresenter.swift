//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation

class GalleryModulePresenter: BasePresenter, GalleryPresenterProtocol {
    var view: GalleryViewProtocol? {
        didSet {
            baseView = view
        }
    }

    var router: GalleryWireframeProtocol? = nil

    let images = ["http://mtdata.ru/u2/photoEE5F/20375776776-0/original.jpg",
                  "https://i.ytimg.com/vi/Py9uWbhA2A8/maxresdefault.jpg",
                  "https://pp.userapi.com/c417518/v417518985/89a1/oXI9OQVXl9k.jpg",
                  "http://bm.img.com.ua/nxs/img/prikol/images/large/5/4/110345_161767.jpg",
                  "https://i.ytimg.com/vi/PWHDyBoFCiw/maxresdefault.jpg",
                  "http://www.moypesik.ru/images/stories/dog-otr2-0916.jpg",
                  "https://www.zooclub.ge/pictures/image3/ac8a46121a0ef8aeff572d6cb569e893.jpg",
                  "http://live4fun.ru/data/old_pictures/img_20682240_112_0.jpg",
                  "https://photo.boltai.com/wp-content/uploads/sites/11/2015/12/36.jpg",
                  "http://animalworld.com.ua/images/2010/December/Foto/Pes/Pes_26.jpg",
                  "http://img-fotki.yandex.ru/get/4512/kybajido.28/0_5bba9_b2e97c08_XL.jpg",
                  "http://21region.org/uploads/posts/2010-12/thumbs/1293635435_podborka_umnyh_pesikov_null_haski_1.jpg",
                  "http://images.kashamalasha.com/201406090011-sobaka-udivlyaka-kashamalasha-com.jpg",
                  "https://dezinfo.net/images4/image/10.2013/husky_emotions/husky_emotions_02.jpg",
                  "https://ru5.anyfad.com/items/t1@95459726-657e-4d88-abb8-41d2671efebe/Milye-Pyosiki-.jpg"]

    
    override func viewDidLoad(view: BaseViewProtocol) {
        super.viewDidLoad(view: view)
         self.view?.showImages(images: images)
    }

    func onDidSelectImage(index: Int) {
        router?.navigateToImageViewer(images: images, selectedImageIndex: index)
    }
}
