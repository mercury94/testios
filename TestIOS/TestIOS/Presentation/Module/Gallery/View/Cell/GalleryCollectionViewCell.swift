//
//  GalleryCollectionViewCell.swift
//  TestIOS
//
//  Created by Nikita on 10.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import SDWebImage

class GalleryCollectionViewCell: UICollectionViewCell {

    public static let Identifier: String = "GalleryCollectionViewCell"


    @IBOutlet weak var imageView: UIImageView!

    func configureCell(imageUrl: String) {
        imageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: R.image.placeholder_dogJpg())
    }

}
