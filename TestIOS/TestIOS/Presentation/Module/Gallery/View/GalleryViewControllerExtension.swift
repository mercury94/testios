//
// Created by Nikita on 10.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit

//Расширение отвечает за предоставление отображение ифнормации
extension GalleryViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        let nibCell = UINib(nibName: "GalleryCollectionViewCell", bundle: Bundle.main)
        collectionView.register(nibCell, forCellWithReuseIdentifier: GalleryCollectionViewCell.Identifier)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images == nil ? 0 : images!.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.Identifier, for: indexPath) as! GalleryCollectionViewCell
        cell.configureCell(imageUrl: images![indexPath.row])
        cell.backgroundColor = UIColor.white
        return cell
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:
            UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let picDimension = self.view.frame.size.width / 3.5
        return CGSize(width: picDimension, height: picDimension)
    }

    func showImages(images: [String]) {
        self.images = images
        collectionView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let leftRightInset = self.view.frame.size.width / 36
        return UIEdgeInsetsMake(10, leftRightInset, 10, leftRightInset)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.onDidSelectImage(index: indexPath.row)
    }
}