//
//  GalleryViewController.swift
//  TestIOS
//
//  Created by Nikita on 01.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class GalleryViewController: TabBarItemViewController, GalleryViewProtocol {
    var presenter: GalleryPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    @IBOutlet weak var collectionView: UICollectionView!
    var images: [String]? = nil

    override func viewDidLayoutSubviews() {
        //FIXME почему то на версиях < 11 collectionview залазит под navigation bar!!! Вроде решение работает
        guard #available(iOS 11, *) else {
            if let rect = self.navigationController?.navigationBar.frame {
                let y = rect.size.height + rect.origin.y
                collectionView.contentInset = UIEdgeInsetsMake(y, 0, 0, 0)
            }
            return
        }

    }

}
