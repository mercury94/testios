//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

protocol GalleryViewProtocol: BaseViewProtocol {

    var presenter: GalleryPresenterProtocol? { get set }

    func showImages(images: [String])

}

protocol GalleryPresenterProtocol: BasePresenterProtocol {

    var view: GalleryViewProtocol? { get set }

    var router: GalleryWireframeProtocol? { get set }

    func onDidSelectImage(index: Int)
    
}

protocol GalleryWireframeProtocol: BaseWireframeProtocol {

    func navigateToImageViewer(images: [String], selectedImageIndex: Int)

}
