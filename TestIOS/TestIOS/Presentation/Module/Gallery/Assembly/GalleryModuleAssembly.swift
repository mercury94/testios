//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class GalleryModuleAssembly: Assembly {
    func assemble(container: Container) {

        container.register(GalleryPresenterProtocol.self) { _ in
            GalleryModulePresenter()
        }

        container.register(GalleryWireframeProtocol.self) { _ in
            let router = GalleryModuleRouter()
            router.container = container
            return router
        }

        container.register(GalleryViewProtocol.self) { r in
            let viewController = GalleryViewController(nibName: "GalleryViewController", bundle: nil)
            var presenter = r.resolve(GalleryPresenterProtocol.self)
            var router = r.resolve(GalleryWireframeProtocol.self)
            viewController.presenter = presenter
            presenter?.view = viewController
            presenter?.router = router
            router?.viewController = viewController as UIViewController
            return viewController
        }
    }
}
