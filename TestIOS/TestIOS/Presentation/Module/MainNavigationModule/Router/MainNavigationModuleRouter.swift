//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class MainNavigationModuleRouter: MainNavigationWireframeProtocol {
    var container: Container? = nil
    var viewController: UIViewController? = nil
    var containerView: UIView? = nil
    var currentViewControllerInContainer: UIViewController? = nil

    func navigateToListScreen() {
        let listViewController = container?.resolve(ListViewProtocol.self)! as! UIViewController
        displayViewController(viewController: listViewController)
    }

    func navigateToGalleryScreen() {
        let listViewController = container?.resolve(GalleryViewProtocol.self)! as! UIViewController
        displayViewController(viewController: listViewController)
    }

    func navigateToServiceScreen() {
        let listViewController = container?.resolve(ServiceViewProtocol.self)! as! UIViewController
        displayViewController(viewController: listViewController)
    }

    private func displayViewController(viewController: UIViewController) {
        if currentViewControllerInContainer != nil {
            hideCurrentViewControllerInContainer()
        }

        self.viewController?.addChildViewController(viewController)
        viewController.view?.frame = containerView!.bounds
        containerView?.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self.viewController)
        currentViewControllerInContainer = viewController
    }

    private func hideCurrentViewControllerInContainer() {
        currentViewControllerInContainer?.willMove(toParentViewController: nil)
        currentViewControllerInContainer?.view.removeFromSuperview()
        currentViewControllerInContainer?.removeFromParentViewController()
        currentViewControllerInContainer = nil
    }

}
