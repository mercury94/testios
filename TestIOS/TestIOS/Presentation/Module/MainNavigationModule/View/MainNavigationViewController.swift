//
//  MainNavigationViewController.swift
//  TestIOS
//
//  Created by Nikita on 01.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class MainNavigationViewController: BaseViewController, MainNavigationViewProtocol, UITabBarDelegate {

    @IBOutlet weak var contentView: UIView! {
        didSet {
            presenter?.router?.containerView = contentView
        }
    }
    
    @IBOutlet weak var bottomBar: UITabBar!

    var presenter: MainNavigationPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    private enum Tabs: Int {
        case List = 0
        case Gallery
        case Service
    }

    override func initViews() {
        bottomBar.delegate = self
        setupNavigationBar()
        bottomBar.items![0].title = R.string.localizable.list()
        bottomBar.items![1].title = R.string.localizable.gallery()
        bottomBar.items![2].title = R.string.localizable.service()
    }

    private func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor.defaultApplicationColor
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
    }

    func selectTabList() {
        bottomBar.selectedItem = bottomBar.items![Tabs.List.rawValue] as UITabBarItem
    }

    func selectTabGallery() {
        bottomBar.selectedItem = bottomBar.items![Tabs.Gallery.rawValue] as UITabBarItem
    }

    func selectTabService() {
        bottomBar.selectedItem = bottomBar.items![Tabs.Service.rawValue] as UITabBarItem
    }

    override func setTitle(title: String) {
        let titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.text = title
        titleLabel.font = UIFont.appFontRegular(withSize: 25.0)
        titleLabel.sizeToFit()
        navigationItem.titleView = titleLabel
    }

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = tabBar.items!.index(of: item)
        if index == Tabs.List.rawValue {
            presenter?.didSelectedTabList()
        } else if index == Tabs.Gallery.rawValue {
            presenter?.didSelectedTabGallery()
        } else if index == Tabs.Service.rawValue {
            presenter?.didSelectedTabService()
        }
    }


}
