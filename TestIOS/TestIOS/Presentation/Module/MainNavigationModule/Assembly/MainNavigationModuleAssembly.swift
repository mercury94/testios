//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import Swinject

class MainNavigationModuleAssembly: Assembly {

    func assemble(container: Container) {
        container.register(MainNavigationPresenterProtocol.self) { _ in
            MainNavigationModulePresenter()
        }

        container.register(MainNavigationWireframeProtocol.self) { _ in
            let router = MainNavigationModuleRouter()
            router.container = container
            return router
        }

        container.register(MainNavigationViewProtocol.self) { r in
            let viewProtocol = MainNavigationViewController(nibName: "MainNavigationViewController", bundle: nil)
            var presenter = r.resolve(MainNavigationPresenterProtocol.self)
            var router = r.resolve(MainNavigationWireframeProtocol.self)
            viewProtocol.presenter = presenter
            presenter?.view = viewProtocol
            presenter?.router = router
            router?.viewController = viewProtocol as UIViewController
            return viewProtocol
        }
    }
}
