//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit

protocol MainNavigationViewProtocol: BaseViewProtocol {

    var presenter: MainNavigationPresenterProtocol? { get set }

    func selectTabList()

    func selectTabGallery()

    func selectTabService()
}

protocol MainNavigationPresenterProtocol: BasePresenterProtocol {

    var view: MainNavigationViewProtocol? { get set }

    var router: MainNavigationWireframeProtocol? { get set }

    func didSelectedTabList()

    func didSelectedTabGallery()

    func didSelectedTabService()
}

protocol MainNavigationWireframeProtocol: BaseWireframeProtocol {

    var containerView: UIView? { get set }

    func navigateToListScreen()

    func navigateToGalleryScreen()

    func navigateToServiceScreen()

}


