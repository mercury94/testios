//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

class MainNavigationModulePresenter: BasePresenter, MainNavigationPresenterProtocol {

    var view: MainNavigationViewProtocol? {
        didSet {
            baseView = view
        }
    }

    var router: MainNavigationWireframeProtocol? = nil

    override func viewDidLoad(view: BaseViewProtocol) {
        super.viewDidLoad(view: view)
        self.view?.selectTabList()
        router?.navigateToListScreen()
    }

    func didSelectedTabList() {
        router?.navigateToListScreen()
    }

    func didSelectedTabGallery() {
        router?.navigateToGalleryScreen()
    }

    func didSelectedTabService() {
        router?.navigateToServiceScreen()
    }
}
