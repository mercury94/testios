//
// Created by Nikita on 10.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//


protocol ImageViewerViewProtocol: BaseViewProtocol {

    var presenter: ImageViewerPresenterProtocol? { get set }

    func showImages(images: [String], firstShowImageIndex: Int)

}

protocol ImageViewerPresenterProtocol: BasePresenterProtocol {

    var view: ImageViewerViewProtocol? { get set }

    var router: ImageViewerWireframeProtocol? { get set }

    var images: [String]? { get set }

    var firstShowImageIndex: Int { get set }

}

protocol ImageViewerWireframeProtocol: BaseWireframeProtocol {


}

