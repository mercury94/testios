//
// Created by Nikita on 10.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class ImageViewerAssembly: Assembly {

    func assemble(container: Container) {


        container.register(ImageViewerPresenterProtocol.self) { _ in
            ImageViewerModulePresenter()
        }

        container.register(ImageViewerWireframeProtocol.self) { _ in
            let router = ImageViewerRouter()
            router.container = container
            return router
        }

        container.register(ImageViewerViewProtocol.self) { r in
            let viewController = ImageViewerController(nibName: "ImageViewerController", bundle: nil)
            var presenter = r.resolve(ImageViewerPresenterProtocol.self)
            var router = r.resolve(ImageViewerWireframeProtocol.self)
            viewController.presenter = presenter
            presenter?.view = viewController
            presenter?.router = router
            router?.viewController = viewController as UIViewController
            return viewController
        }

    }
}
