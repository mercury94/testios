//
// Created by Nikita on 10.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class ImageViewerRouter : ImageViewerWireframeProtocol {

    var container: Swinject.Container? = nil
    var viewController: UIKit.UIViewController? = nil
}
