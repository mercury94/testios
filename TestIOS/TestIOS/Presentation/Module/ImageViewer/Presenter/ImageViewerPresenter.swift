//
// Created by Nikita on 10.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation

class ImageViewerModulePresenter: BasePresenter, ImageViewerPresenterProtocol {
    var view: ImageViewerViewProtocol? {
        didSet {
            baseView = view
        }
    }
    var router: ImageViewerWireframeProtocol? = nil
    var images: [String]? = nil
    var firstShowImageIndex: Int = 0

    override func viewDidLoad(view: BaseViewProtocol) {
        super.viewDidLoad(view: view)
        self.view?.showImages(images: images!, firstShowImageIndex: firstShowImageIndex)
    }

}
