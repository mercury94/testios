//
//  ImageViewController.swift
//  TestIOS
//
//  Created by Nikita on 10.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import SDWebImage

class ImageViewController: UIViewController, UIScrollViewDelegate {

    var imageUrl: String? = nil
    var indexImage: Int = 0

    private let maximumImageZoomScale = CGFloat(5.0)
    private let minimumImageZoomScale = CGFloat(1.0)
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        scrollView.maximumZoomScale = maximumImageZoomScale
        scrollView.minimumZoomScale = minimumImageZoomScale
        imageView.sd_setImage(with: URL(string: imageUrl!), placeholderImage: R.image.placeholder_dogJpg())
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
