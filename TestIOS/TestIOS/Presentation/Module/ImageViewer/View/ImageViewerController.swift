//
//  ImageViewerController.swift
//  TestIOS
//
//  Created by Nikita on 10.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class ImageViewerController: BasePageViewController, ImageViewerViewProtocol, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    var presenter: ImageViewerPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    var pageControl = UIPageControl()
    var images: [String]? = nil

    func showImages(images: [String], firstShowImageIndex: Int) {
        self.images = images
        let firstImageUrl = self.images![firstShowImageIndex];
        setViewControllers([newVc(index: firstShowImageIndex, imageUrl: firstImageUrl)],
                direction: .forward,
                animated: true,
                completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
    }


    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        for view in view.subviews {
            if view is UIScrollView {
                view.frame = UIScreen.main.bounds // Why? I don't know.
            } else if view is UIPageControl {
                view.backgroundColor = UIColor.clear
            }
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configurePageControl()

        if #available(iOS 11, *) {
            //В iOS 11  так и не получилось сделать прозрачный navigation bar
        } else {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if #available(iOS 11, *) {

        } else {
            self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
            self.navigationController?.navigationBar.shadowImage = nil
        }
    }

    func newVc(index: Int, imageUrl: String) -> UIViewController {
        let viewController = ImageViewController(nibName: "ImageViewController", bundle: nil)
        viewController.imageUrl = imageUrl
        viewController.indexImage = index
        return viewController
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController is ImageViewController {

            let imageViewController = viewController as! ImageViewController
            let previousIndex = imageViewController.indexImage - 1
            guard previousIndex >= 0 else {
                return nil
            }

            guard images!.count > previousIndex else {
                return nil
            }

            return newVc(index: previousIndex, imageUrl: images![previousIndex])
        }

        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        if viewController is ImageViewController {
            let imageViewController = viewController as! ImageViewController

            let nextIndex = imageViewController.indexImage + 1
            let orderedViewControllersCount = images!.count

            guard orderedViewControllersCount != nextIndex else {
                return nil
            }

            guard orderedViewControllersCount > nextIndex else {
                return nil
            }

            return newVc(index: nextIndex, imageUrl: images![nextIndex])
        }
        return nil

    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if pageViewController is ImageViewController {
            let imageViewController = pageViewController as! ImageViewController
            self.pageControl.currentPage = imageViewController.indexImage
        }
    }

    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        self.pageControl.numberOfPages = images == nil ? 0 : images!.count
        self.pageControl.currentPage = self.presenter!.firstShowImageIndex
        self.pageControl.tintColor = UIColor.gray
        self.pageControl.pageIndicatorTintColor = UIColor.white
        self.pageControl.currentPageIndicatorTintColor = UIColor.gray
        self.view.addSubview(pageControl)
    }

}
