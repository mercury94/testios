//
// Created by Nikita on 29.09.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SplashModulePresenter: BasePresenter, SplashPresenterProtocol {

    private let splashDelaySeconds = 3.0
    var view: SplashViewProtocol? {
        didSet {
            baseView = view
        }
    }

    var router: SplashWireframeProtocol?

    override func viewDidLoad(view: BaseViewProtocol) {
        super.viewDidLoad(view: view)
        
        Observable.just(true)
            .delay(splashDelaySeconds, scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: { self.router?.navigateToMainScreen() })
    }
}



