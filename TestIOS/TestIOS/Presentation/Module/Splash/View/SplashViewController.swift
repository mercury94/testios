//
//  SplashViewController.swift
//  TestIOS
//
//  Created by Nikita on 29.09.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

extension SplashViewProtocol {

}

class SplashViewController: BaseViewController, SplashViewProtocol {

    var presenter: SplashPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

}

