//
//  SplashModuleRouter.swift
//  TestIOS
//
//  Created by Nikita on 29.09.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import Foundation
import UIKit
import Swinject

class SplashModuleRouter: SplashWireframeProtocol {

    var viewController: UIViewController? = nil
    var container: Container? = nil

    func navigateToMainScreen() {
        let mainViewController = container!.resolve(MainNavigationViewProtocol.self)! as! UIViewController
        let navController = TestIOSNavigationController(rootViewController: mainViewController)
        viewController!.present(navController, animated: true, completion: nil)
    }
}

