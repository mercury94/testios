//
// Created by Nikita on 29.09.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import Swinject
import UIKit


class SplashModuleAssembly: Assembly {

    func assemble(container: Container) {
        
        container.register(SplashPresenterProtocol.self) { _ in
            SplashModulePresenter()
        }

        container.register(SplashWireframeProtocol.self) { _ in
            let router = SplashModuleRouter()
            router.container = container
            return router
        }

        container.register(SplashViewProtocol.self) { r in
            let splashViewProtocol = SplashViewController(nibName: "SplashViewController", bundle: nil)
            var presenter = r.resolve(SplashPresenterProtocol.self)
            var router = r.resolve(SplashWireframeProtocol.self)
            splashViewProtocol.presenter = presenter
            presenter?.view = splashViewProtocol
            presenter?.router = router
            router?.viewController = splashViewProtocol as UIViewController
            return splashViewProtocol
        }
    }
}
