//
//  SplashModuleContract.swift
//  TestIOS
//
//  Created by Nikita on 29.09.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import Foundation

protocol SplashViewProtocol: BaseViewProtocol {

    var presenter: SplashPresenterProtocol? { get set }
}

protocol SplashPresenterProtocol: BasePresenterProtocol {

    var view: SplashViewProtocol? { get set }

    var router: SplashWireframeProtocol? { get set }
}

protocol SplashWireframeProtocol: BaseWireframeProtocol {

    func navigateToMainScreen();
}

