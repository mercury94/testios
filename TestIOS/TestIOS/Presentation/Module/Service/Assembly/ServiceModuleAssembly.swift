//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject
import RxSwift

class ServiceModuleAssembly: Assembly {

    func assemble(container: Container) {


        container.register(FetchQuotesInteractor.self) { r in
            return FetchQuotesInteractor(
                    quoteRepository: r.resolve(QuoteRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.serialScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(ServicePresenterProtocol.self) { r in
            let presenter = ServiceModulePresenter()
            presenter.fetchQuotesInteractor = r.resolve(FetchQuotesInteractor.self)
            return presenter
        }

        container.register(ServiceWireframeProtocol.self) { _ in
            let router = ServiceModuleRouter()
            router.container = container
            return router
        }

        container.register(ServiceViewProtocol.self) { r in
                    let viewController = ServiceViewController(nibName: "ServiceViewController", bundle: nil)
                    var presenter = r.resolve(ServicePresenterProtocol.self)
                    var router = r.resolve(ServiceWireframeProtocol.self)
                    viewController.presenter = presenter
                    presenter?.view = viewController
                    presenter?.router = router
                    router?.viewController = viewController as UIViewController
                    return viewController
                }
                .inObjectScope(.container)
    }
}
