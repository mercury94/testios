//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

protocol ServiceViewProtocol: BaseViewProtocol {

    var presenter: ServicePresenterProtocol? { get set }

    func showData(quotes: Array<Quote>)

    func showEmptyView()

}

protocol ServicePresenterProtocol: BasePresenterProtocol {

    var view: ServiceViewProtocol? { get set }

    var router: ServiceWireframeProtocol? { get set }

    var fetchQuotesInteractor: FetchQuotesInteractor? { get set }
}

protocol ServiceWireframeProtocol: BaseWireframeProtocol {

}