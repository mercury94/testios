//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit
import Swinject

class ServiceModuleRouter: ServiceWireframeProtocol {
    var container: Container? = nil
    var viewController: UIViewController? = nil
}
