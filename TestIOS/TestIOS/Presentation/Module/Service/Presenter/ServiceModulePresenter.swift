//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//


class ServiceModulePresenter: BasePresenter, ServicePresenterProtocol {
    var view: ServiceViewProtocol? {
        didSet {
            baseView = view
        }
    }
    var router: ServiceWireframeProtocol? = nil
    var fetchQuotesInteractor: FetchQuotesInteractor? = nil

    override func viewDidLoad(view: BaseViewProtocol) {
        super.viewDidLoad(view: view)
    }

    override func viewDidAppear(view: BaseViewProtocol) {
        super.viewDidAppear(view: view)
        fetchQuotes()
    }

    private func fetchQuotes() {
        fetchQuotesInteractor?.execute(onSubscribe: doOnSubscribeFetchQuotes,
                onNext: doOnNextFetchQuotes,
                onError: doOnErrorFetchQuotes,
                onDisposed: doOnDisposedFetchQuotes)
    }

    private func doOnSubscribeFetchQuotes() {
        view?.showPending()
    }

    private func doOnErrorFetchQuotes(error: Error) {
        view?.hidePending()
    }

    private func doOnNextFetchQuotes(quotes: Array<Quote>) {
        if quotes.isEmpty {
            view?.showEmptyView()
        } else {
            view?.showData(quotes: quotes)
        }
    }

    private func doOnDisposedFetchQuotes() {
        view?.hidePending()
    }

}
