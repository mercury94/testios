//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit

class QuoteTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {

    private var quotes: Array<Quote>? = nil

    init(quotes: Array<Quote>? = Array()) {
        self.quotes = quotes
    }

    func showQuotes(quotes: [Quote] = Array()) {
        self.quotes = quotes
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quotes!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ServiceTableViewCell.Identifier, for: indexPath) as! ServiceTableViewCell
        cell.configureCell(quote: quotes![indexPath.row])
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

}
