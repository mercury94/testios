//
//  ServiceViewController.swift
//  TestIOS
//
//  Created by Nikita on 01.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class ServiceViewController: TabBarItemViewController, ServiceViewProtocol {
    var presenter: ServicePresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    private var dataSource: QuoteTableViewDataSource?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyView: UILabel!

    override func initViews() {
        super.initViews()
        dataSource = QuoteTableViewDataSource()
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 40
        tableView.rowHeight = UITableViewAutomaticDimension;
        let nibCell = UINib(nibName: "ServiceTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: ServiceTableViewCell.Identifier)
        emptyView.isHidden = true
        emptyView.text = R.string.localizable.emptyQuoteList()
        tableView.allowsSelection = false
    }

    override func viewDidLayoutSubviews() {

        //FIXME почему то на версиях < 11 tableview залазит под navigation bar!!! Вроде решение работает
        guard #available(iOS 11, *) else {
            if let rect = self.navigationController?.navigationBar.frame {
                let y = rect.size.height + rect.origin.y
                self.tableView.contentInset = UIEdgeInsetsMake(y, 0, 0, 0)
            }
            return
        }
    }


    func showData(quotes: Array<Quote>) {
        emptyView.isHidden = true
        dataSource?.showQuotes(quotes: quotes)
        tableView.reloadData()
    }

    func showEmptyView() {
        emptyView.isHidden = false
    }

}
