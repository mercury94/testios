//
//  ServiceTableViewCell.swift
//  TestIOS
//
//  Created by Nikita on 08.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    public static let Identifier: String = "ServiceTableViewCell"

    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!


    func configureCell(quote: Quote) {
        descriptionTextView.text = quote.description!
        ratingLabel.text = "\(R.string.localizable.quoteRating()): \(quote.rating!)"
        dateTimeLabel.text = "\(R.string.localizable.quoteDateTime()): \(quote.time!)"
    }

}
