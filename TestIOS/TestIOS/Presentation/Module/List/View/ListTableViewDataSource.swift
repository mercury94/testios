//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import UIKit

class ListTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {

    private var workers: Array<Worker>
    private var leaders: Array<Leader>
    private var accountants: Array<Accountant>
    
    //didSelectEvents
    var onDidSelectWorkerEvent: ((_ index: Int, _  worker: Worker) -> ())? = nil
    var onDidSelectLeaderEvent: ((_ index: Int, _  worker: Leader) -> ())? = nil
    var onDidSelectAccountantEvent: ((_ index: Int, _  worker: Accountant) -> ())? = nil
    
    //onClickRemoveEvents
    var onClickRemoveWorkerEvent: ((_ indexPath: IndexPath) -> ())? = nil
    var onClickRemoveLeaderEvent: ((_ indexPath: IndexPath) -> ())? = nil
    var onClickRemoveAccountantEvent: ((_ indexPath: IndexPath) -> ())? = nil
    
    //onMoveEvents
    var onMoveLeaderEvent: ((_ to: Int, _ from: Int) -> ())? = nil
    var onMoveWorkerEvent: ((_ to: Int, _ from: Int) -> ())? = nil
    var onMoveAccountantEvent: ((_ to: Int, _ from: Int) -> ())? = nil

    init(workers: Array<Worker> = Array(), leaders: Array<Leader> = Array(), accountants: Array<Accountant> = Array()) {
        self.workers = workers
        self.leaders = leaders
        self.accountants = accountants
    }

    func showData(workers: Array<Worker> = Array(), leaders: Array<Leader> = Array(), accountants: Array<Accountant> = Array()) {
        self.workers = workers
        self.leaders = leaders
        self.accountants = accountants
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if indexPath.section == 0 {
            if leaders.isEmpty {
                if workers.isEmpty {
                    cell = tableView.dequeueReusableCell(withIdentifier: AccountantTableViewCell.Identifier, for: indexPath)
                    (cell as! AccountantTableViewCell).configureCell(accountant: accountants[indexPath.row])
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: WorkerTableViewCell.Identifier, for: indexPath)
                    (cell as! WorkerTableViewCell).configureCell(worker: workers[indexPath.row])
                }
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: LeaderTableViewCell.Identifier, for: indexPath)
                (cell as! LeaderTableViewCell).configureCell(leader: leaders[indexPath.row])
            }
        } else if indexPath.section == 1 {
            if workers.isEmpty {
                cell = tableView.dequeueReusableCell(withIdentifier: AccountantTableViewCell.Identifier, for: indexPath)
                (cell as! AccountantTableViewCell).configureCell(accountant: accountants[indexPath.row])
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: WorkerTableViewCell.Identifier, for: indexPath)
                (cell as! WorkerTableViewCell).configureCell(worker: workers[indexPath.row])
            }
        } else if indexPath.section == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: AccountantTableViewCell.Identifier, for: indexPath)
            (cell as! AccountantTableViewCell).configureCell(accountant: accountants[indexPath.row])
        }

        //если все прaвильно расчитано, то cell всегда != nil
        return cell == nil ? tableView.dequeueReusableCell(withIdentifier: ListTableViewCell.Identifier, for: indexPath) : cell!
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        //вычисление кол-во сотрудников в секции
        if leaders.isEmpty {
            if workers.isEmpty {
                count = accountants.count
            } else {
                if section == 0 {
                    count = workers.count
                } else {
                    count = accountants.count
                }
            }
        } else {
            if workers.isEmpty {
                if (section == 0) {
                    count = leaders.count
                } else {
                    count = accountants.count
                }
            } else {
                if section == 0 {
                    count = leaders.count
                } else if section == 1 {
                    count = workers.count
                } else {
                    count = accountants.count
                }
            }
        }
        return count
    }

    
    //Определяем заголовок для секции
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title = ""
        if leaders.isEmpty {
            if workers.isEmpty {
                title = R.string.localizable.accountants()
            } else {
                if section == 0 {
                    title = R.string.localizable.workers()
                } else {
                    title = R.string.localizable.accountants()
                }
            }
        } else {
            if workers.isEmpty {
                if (section == 0) {
                    title = R.string.localizable.leaderships()
                } else {
                    title = R.string.localizable.accountants()
                }
            } else {
                if section == 0 {
                    title = R.string.localizable.leaderships()
                } else if section == 1 {
                    title =  R.string.localizable.workers()
                } else {
                    title = R.string.localizable.accountants()
                }
            }
        }
        return title
    }

    //Вычисляется коль-во секции, пустые секции не должны отображаться
    func numberOfSections(in tableView: UITableView) -> Int {
        var number = 0
        if !leaders.isEmpty {
            number = number + 1
        }

        if !workers.isEmpty {
            number = number + 1
        }

        if !accountants.isEmpty {
            number = number + 1
        }

#if DEBUG
        print("numberOfSections = \(number)")
#endif
        return number
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        // Выбран сотрудник для удаления
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            //Вычисление выбранного сотрудника по IndexPath
            if indexPath.section == 0 {
                if leaders.isEmpty {
                    if workers.isEmpty {
                        onClickRemoveAccountantEvent?(indexPath)
                    } else {
                        onClickRemoveWorkerEvent?(indexPath)
                    }
                } else {
                    onClickRemoveLeaderEvent?(indexPath)
                }
            } else if indexPath.section == 1 {
                if workers.isEmpty {
                    onClickRemoveAccountantEvent?(indexPath)
                } else {
                    onClickRemoveWorkerEvent?(indexPath)
                }
            } else if indexPath.section == 2 {
                onClickRemoveAccountantEvent?(indexPath)
            }
        }
    }

    //Выбран сотрудник для редактироования или просмотра
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if leaders.isEmpty {
                if workers.isEmpty {
                    onDidSelectAccountantEvent?(indexPath.row, accountants[indexPath.row])
                } else {
                    onDidSelectWorkerEvent?(indexPath.row, workers[indexPath.row])
                }
            } else {
                onDidSelectLeaderEvent?(indexPath.row, leaders[indexPath.row])
            }
        } else if indexPath.section == 1 {
            if workers.isEmpty {
                onDidSelectAccountantEvent?(indexPath.row, accountants[indexPath.row])
            } else {
                onDidSelectWorkerEvent?(indexPath.row, workers[indexPath.row])
            }
        } else if indexPath.section == 2 {
            onDidSelectAccountantEvent?(indexPath.row, accountants[indexPath.row])
        }
    }

    //Удаление сотрудника из списка
    func deleteWorkerByIndexPath(_ indexPath: IndexPath, tableView: UITableView) {
        workers.remove(at: indexPath.row)
        tableView.beginUpdates()
        if !workers.isEmpty {
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else {
            tableView.deleteSections(IndexSet(integer: indexPath.section), with: .automatic)
        }
        tableView.endUpdates()
    }

    //Удаление руководителя из списка
    func deleteLeaderByIndexPath(_ indexPath: IndexPath, tableView: UITableView) {
        leaders.remove(at: indexPath.row)
        tableView.beginUpdates()
        if !leaders.isEmpty {
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else {
            tableView.deleteSections(IndexSet(integer: indexPath.section), with: .automatic)
        }
        tableView.endUpdates()
    }

    //Удаление бухгалтера из списка
    func deleteAccountantByIndexPath(_ indexPath: IndexPath, tableView: UITableView) {
        accountants.remove(at: indexPath.row)
        tableView.beginUpdates()
        if !accountants.isEmpty {
            tableView.deleteRows(at: [indexPath], with: .automatic)
        } else {
            tableView.deleteSections(IndexSet(integer: indexPath.section), with: .automatic)
        }
        tableView.endUpdates()
    }

     //Разрешение чтобы перемещать всех сотрудников
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    //Применеие изменений после перемещения сотрудников
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if sourceIndexPath.row == destinationIndexPath.row {
            return
        }

        //Обвноление списка путем удаление сотрудника по его индексу и вставки с новым индексом
        if sourceIndexPath.section == 0 {
            if leaders.isEmpty {
                if workers.isEmpty {
                    let accountant = accountants.remove(at: sourceIndexPath.row)
                    accountants.insert(accountant, at: destinationIndexPath.row)
                    onMoveAccountantEvent?(destinationIndexPath.row, sourceIndexPath.row)
                } else {
                    let worker = workers.remove(at: sourceIndexPath.row)
                    workers.insert(worker, at: destinationIndexPath.row)
                    onMoveWorkerEvent?(destinationIndexPath.row, sourceIndexPath.row)
                }
            } else {
                let leader = leaders.remove(at: sourceIndexPath.row)
                leaders.insert(leader, at: destinationIndexPath.row)
                onMoveLeaderEvent?(destinationIndexPath.row, sourceIndexPath.row)
            }
        } else if sourceIndexPath.section == 1 {
            if workers.isEmpty {
                let accountant = accountants.remove(at: sourceIndexPath.row)
                accountants.insert(accountant, at: destinationIndexPath.row)
                onMoveAccountantEvent?(destinationIndexPath.row, sourceIndexPath.row)
            } else {
                let worker = workers.remove(at: sourceIndexPath.row)
                workers.insert(worker, at: destinationIndexPath.row)
                onMoveWorkerEvent?(destinationIndexPath.row, sourceIndexPath.row)
            }
        } else if sourceIndexPath.section == 2 {
            let accountant = accountants.remove(at: sourceIndexPath.row)
            accountants.insert(accountant, at: destinationIndexPath.row)
            onMoveAccountantEvent?(destinationIndexPath.row, sourceIndexPath.row)
        }
    }

    //Разрешение перемещать сотрудника в выбранную позицию
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath,
                   toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        //разрешается перемещать только в пределах текущей секции
        return sourceIndexPath.section == proposedDestinationIndexPath.section ? proposedDestinationIndexPath : sourceIndexPath
    }

}
