//
//  ListViewController.swift
//  TestIOS
//
//  Created by Nikita on 01.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class ListViewController: TabBarItemViewController, ListViewProtocol {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyListLabel: UILabel!

    private var editBtnItem: UIBarButtonItem?
    private var employeeTableViewDataSource: ListTableViewDataSource?
    var presenter: ListPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    override func initViews() {
        super.initViews()
        employeeTableViewDataSource = ListTableViewDataSource()
        tableView.dataSource = employeeTableViewDataSource
        tableView.delegate = employeeTableViewDataSource
        tableView.rowHeight = UITableViewAutomaticDimension;
        tableView.estimatedRowHeight = 40;

        tableView.tableFooterView = UIView()

        var nibCell = UINib(nibName: "ListTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: ListTableViewCell.Identifier)

        nibCell = UINib(nibName: "LeaderTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: LeaderTableViewCell.Identifier)

        nibCell = UINib(nibName: "WorkerTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: WorkerTableViewCell.Identifier)

        nibCell = UINib(nibName: "AccountantTableViewCell", bundle: Bundle.main)
        tableView.register(nibCell, forCellReuseIdentifier: AccountantTableViewCell.Identifier)

        employeeTableViewDataSource?.onDidSelectWorkerEvent = presenter?.onDidSelectWorker
        employeeTableViewDataSource?.onDidSelectAccountantEvent = presenter?.onDidSelectAccountant
        employeeTableViewDataSource?.onDidSelectLeaderEvent = presenter?.onDidSelectLeader

        employeeTableViewDataSource?.onClickRemoveWorkerEvent = presenter?.onClickRemoveWorker
        employeeTableViewDataSource?.onClickRemoveAccountantEvent = presenter?.onClickRemoveAccountant
        employeeTableViewDataSource?.onClickRemoveLeaderEvent = presenter?.onClickRemoveLeader

        employeeTableViewDataSource?.onMoveLeaderEvent = presenter?.onMoveLeader
        employeeTableViewDataSource?.onMoveWorkerEvent = presenter?.onMoveWorker
        employeeTableViewDataSource?.onMoveAccountantEvent = presenter?.onMoveAccountant

        emptyListLabel.text = R.string.localizable.emptyListWorkers()

    }


    override func viewDidLayoutSubviews() {

        //FIXME почему то на версиях < 11 tableview залазит под navigation bar!!! Вроде решение работает
        guard #available(iOS 11, *) else {
            if let rect = self.navigationController?.navigationBar.frame {
                let y = rect.size.height + rect.origin.y
                self.tableView.contentInset = UIEdgeInsetsMake(y, 0, 0, 0)
            }
            return
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        parent?.navigationItem.rightBarButtonItems = [createRightBarButtonItem()!, createSortButton()]
    }

    override func createRightBarButtonItem() -> UIBarButtonItem? {
        return UIBarButtonItem(image: UIImage(imageLiteralResourceName: "add_icon.png"),
                style: UIBarButtonItemStyle.done,
                target: self,
                action: #selector(self.clickedBtnAddEmployee(sender:)))
    }

    private func createSortButton() -> UIBarButtonItem {
        return UIBarButtonItem(image: UIImage(imageLiteralResourceName: "ic_sort_ascending.png"),
                style: UIBarButtonItemStyle.done,
                target: self,
                action: #selector(self.clickedBtnSortEmployees(sender:)))
    }

    func showEmployees(accountants: Array<Accountant>, leaders: Array<Leader>, workers: Array<Worker>) {
        tableView.isHidden = false
        employeeTableViewDataSource?.showData(workers: workers, leaders: leaders, accountants: accountants)
        tableView.reloadData()
    }

    func showEditBtn() {
        parent?.navigationItem.leftBarButtonItem = UIBarButtonItem(title: R.string.localizable.edit(),
                style: UIBarButtonItemStyle.done,
                target: self,
                action: #selector(self.clickedBtnBeginEditList(sender:)))
    }

    func hideEditBtn() {
        parent?.navigationItem.leftBarButtonItem = nil

    }

    func showDoneBtn() {
        parent?.navigationItem.leftBarButtonItem = UIBarButtonItem(title: R.string.localizable.done(),
                style: UIBarButtonItemStyle.done,
                target: self,
                action: #selector(self.clickedBtnDoneEditList(sender:)))
    }

    func hideDoneBtn() {
        parent?.navigationItem.leftBarButtonItem = nil
    }

    @objc func clickedBtnBeginEditList(sender: UIButton) {
#if DEBUG
        print("clickedBtnBeginEditList")
#endif
        presenter?.onClickedBtnEdit()
    }

    @objc func clickedBtnDoneEditList(sender: UIButton) {
#if DEBUG
        print("clickedBtnDoneEditList")
#endif
        presenter?.onClickedBtnDone()
    }

    @objc func clickedBtnAddEmployee(sender: UIButton) {
#if DEBUG
        print("clickedBtnAddEmployee")
#endif
        presenter?.onClickedBtnAdd()
    }

    @objc func clickedBtnSortEmployees(sender: UIButton) {
#if DEBUG
        print("clickedBtnSortEmployees")
#endif
        presenter?.onClickedBtnSort()
    }

    func setEnableEditMode() {
        tableView?.setEditing(true, animated: true)
        tableView.reloadData()
    }

    func setDisableEditMode() {
        tableView?.setEditing(false, animated: true)
        tableView.reloadData()
    }

    func hideList() {
        tableView.isHidden = true
    }

    func showEmptyView() {
        emptyListLabel.isHidden = false
    }

    func hideEmptyView() {
        emptyListLabel.isHidden = true
    }

    func deleteWorkerWithIndexPath(indexPath: IndexPath) {
        employeeTableViewDataSource?.deleteWorkerByIndexPath(indexPath, tableView: tableView)
    }

    func deleteLeaderWithIndexPath(indexPath: IndexPath) {
        employeeTableViewDataSource?.deleteLeaderByIndexPath(indexPath, tableView: tableView)
    }

    func deleteAccountantWithIndexPath(indexPath: IndexPath) {
        employeeTableViewDataSource?.deleteAccountantByIndexPath(indexPath, tableView: tableView)
    }
    
    func isWasRunEditMode() -> Bool {
        return tableView.isEditing
    }
}
