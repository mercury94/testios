//
//  WorkerTableViewCell.swift
//  TestIOS
//
//  Created by Nikita on 05.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class WorkerTableViewCell: UITableViewCell {

    public static let Identifier: String = "WorkerTableViewCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberWorkplaceLabel: UILabel!
    @IBOutlet weak var lunchTimeLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    
    func configureCell(worker: Worker) {
        nameLabel.text = "\(worker.name!) \(worker.secondName!) \(worker.patronymic!)"
        numberWorkplaceLabel.text = "\(R.string.localizable.numberWorkplaceCompact()): \(worker.numberWorkplace!)"
        lunchTimeLabel.text = "\(R.string.localizable.lunchTimeCompact()): \(worker.lunchBeginTime!) - \(worker.lunchEndTime!)"
        salaryLabel.text = "\(R.string.localizable.salary()): \(worker.salary!)"
    }

}
