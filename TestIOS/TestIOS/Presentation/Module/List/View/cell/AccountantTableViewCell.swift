//
//  AccountantTableViewCell.swift
//  TestIOS
//
//  Created by Nikita on 05.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class AccountantTableViewCell: UITableViewCell {

    public static let Identifier: String = "AccountantTableViewCell"

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var numberWorkplaceLabel: UILabel!

    @IBOutlet weak var lunchTimeLabel: UILabel!

    @IBOutlet weak var salaryLabel: UILabel!

    @IBOutlet weak var typeAccountantLabel: UILabel!

    func configureCell(accountant: Accountant) {
        nameLabel.text = "\(accountant.name!) \(accountant.secondName!) \(accountant.patronymic!)"
        numberWorkplaceLabel.text = "\(R.string.localizable.numberWorkplaceCompact()): \(accountant.numberWorkplace!)"
        lunchTimeLabel.text = "\(R.string.localizable.lunchTimeCompact()): \(accountant.lunchBeginTime!) - \(accountant.lunchEndTime!)"
        salaryLabel.text = "\(R.string.localizable.salary()): \(accountant.salary!)"
        typeAccountantLabel.text = "\(R.string.localizable.typeAccountant()): \(accountant.typeAccountant!)"
    }

}
