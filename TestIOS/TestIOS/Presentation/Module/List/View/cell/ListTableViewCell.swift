//
//  ListTableViewCell.swift
//  TestIOS
//
//  Created by Nikita on 02.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit


//(Nikita) Test cell (does not should use)

class ListTableViewCell: UITableViewCell {

    public static let Identifier: String = "ListTableViewCell"

    @IBOutlet weak var imageItem: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
