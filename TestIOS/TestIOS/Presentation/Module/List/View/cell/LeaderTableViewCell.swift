//
//  LeaderTableViewCell.swift
//  TestIOS
//
//  Created by Nikita on 05.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit

class LeaderTableViewCell: UITableViewCell {

    public static let Identifier: String = "LeaderTableViewCell"

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var businessTimeLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    
    func configureCell(leader: Leader) {
        nameLabel.text = "\(leader.name!) \(leader.secondName!) \(leader.patronymic!)"
        businessTimeLabel.text = " \(R.string.localizable.businessHoursTime()): \(leader.businessHoursBeginTime!) - \(leader.businessHoursEndTime!)"
        salaryLabel.text = "\(R.string.localizable.salary()): \(leader.salary!)"
    }

}
