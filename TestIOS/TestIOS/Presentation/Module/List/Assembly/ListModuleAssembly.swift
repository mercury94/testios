//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject
import RxSwift

class ListModuleAssembly: Assembly {

    func assemble(container: Container) {

        container.register(ListWireframeProtocol.self) { _ in
            let router = ListModuleRouter()
            router.container = container
            return router
        }

        container.register(FetchListEmployeeInteractor.self) { r in
            FetchListEmployeeInteractor(
                    accountantRepository: r.resolve(AccountantRepositoryProtocol.self)!,
                    leaderRepository: r.resolve(LeaderRepositoryProtocol.self)!,
                    workerRepository: r.resolve(WorkerRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.serialScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(SortEmployeesInteractor.self) { r in
            SortEmployeesInteractor(
                    saveWorkerInteractor: r.resolve(SaveWorkerInteractor.self)!,
                    saveLeaderInteractor: r.resolve(SaveLeaderInteractor.self)!,
                    saveAccountantInteractor: r.resolve(SaveAccountantInteractor.self)!,
                    fetchEmployeeListInteractor: r.resolve(FetchListEmployeeInteractor.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.serialScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(SaveWorkerInteractor.self) { r in
            return SaveWorkerInteractor(workerRepository: r.resolve(WorkerRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(SaveLeaderInteractor.self) { r in
            return SaveLeaderInteractor(leaderRepository: r.resolve(LeaderRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(SaveAccountantInteractor.self) { r in
            return SaveAccountantInteractor(accountantRepository: r.resolve(AccountantRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(RemoveWorkerInteractor.self) { r in
            return RemoveWorkerInteractor(workerRepository: r.resolve(WorkerRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(RemoveLeaderInteractor.self) { r in
            return RemoveLeaderInteractor(leaderRepository: r.resolve(LeaderRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(RemoveAccountantInteractor.self) { r in
            return RemoveAccountantInteractor(accountantRepository: r.resolve(AccountantRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(ListPresenterProtocol.self) { r in
            let presenter = ListModulePresenter()
            presenter.fetchEmployeeListInteractor = r.resolve(FetchListEmployeeInteractor.self)
            presenter.sortEmployeesInteractor = r.resolve(SortEmployeesInteractor.self)
            presenter.removeWorkerInteractor = r.resolve(RemoveWorkerInteractor.self)!
            presenter.removeLeaderInteractor = r.resolve(RemoveLeaderInteractor.self)!
            presenter.removeAccountantInteractor = r.resolve(RemoveAccountantInteractor.self)!
            presenter.saveWorkerInteractor = r.resolve(SaveWorkerInteractor.self)!
            presenter.saveLeaderInteractor = r.resolve(SaveLeaderInteractor.self)!
            presenter.saveAccountantInteractor = r.resolve(SaveAccountantInteractor.self)!
            return presenter
        }

        container.register(ListViewProtocol.self) { r in
                    let viewController = ListViewController(nibName: "ListViewController", bundle: nil)
                    var presenter = r.resolve(ListPresenterProtocol.self)
                    var router = r.resolve(ListWireframeProtocol.self)
                    viewController.presenter = presenter
                    presenter?.view = viewController
                    presenter?.router = router
                    router?.viewController = viewController as UIViewController
                    return viewController
                }
                .inObjectScope(.container)
    }
}
