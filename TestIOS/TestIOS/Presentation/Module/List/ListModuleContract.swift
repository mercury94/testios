//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation

protocol ListViewProtocol: BaseViewProtocol {

    var presenter: ListPresenterProtocol? { get set }

    func showEmployees(accountants: Array<Accountant>,
                       leaders: Array<Leader>,
                       workers: Array<Worker>)

    func deleteWorkerWithIndexPath(indexPath: IndexPath)

    func deleteLeaderWithIndexPath(indexPath: IndexPath)

    func deleteAccountantWithIndexPath(indexPath: IndexPath)

    func showEditBtn()

    func hideEditBtn()

    func showDoneBtn()

    func hideDoneBtn()

    func setEnableEditMode()

    func setDisableEditMode()

    func hideList()

    func showEmptyView()

    func hideEmptyView()
    
    func isWasRunEditMode() -> Bool

}

protocol ListPresenterProtocol: BasePresenterProtocol {

    var view: ListViewProtocol? { get set }

    var router: ListWireframeProtocol? { get set }

    var sortEmployeesInteractor: SortEmployeesInteractor? { get set }

    var removeWorkerInteractor: RemoveWorkerInteractor? { get set }

    var removeAccountantInteractor: RemoveAccountantInteractor? { get set }

    var removeLeaderInteractor: RemoveLeaderInteractor? { get set }

    var saveWorkerInteractor: SaveWorkerInteractor? { get set }

    var saveLeaderInteractor: SaveLeaderInteractor? { get set }

    var saveAccountantInteractor: SaveAccountantInteractor? { get set }

    var fetchEmployeeListInteractor: FetchListEmployeeInteractor? { get set }

    func onClickedBtnEdit()

    func onClickedBtnDone()

    func onClickedBtnAdd()

    func onClickedBtnSort()

    func onDidSelectWorker(index: Int, worker: Worker)

    func onDidSelectLeader(index: Int, leader: Leader)

    func onDidSelectAccountant(index: Int, accountant: Accountant)

    func onClickRemoveWorker(indexPath: IndexPath)

    func onClickRemoveLeader(indexPath: IndexPath)

    func onClickRemoveAccountant(indexPath: IndexPath)

    func onMoveLeader(to: Int, from: Int)

    func onMoveWorker(to: Int, from: Int)

    func onMoveAccountant(to: Int, from: Int)

}

protocol ListWireframeProtocol: BaseWireframeProtocol {

    func navigateToAddEmployeeScreen()

    func navigateToEditEmployeeScreen(employee: Employee)

}

