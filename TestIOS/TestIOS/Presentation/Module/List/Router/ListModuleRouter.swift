//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit
import Swinject

class ListModuleRouter: ListWireframeProtocol {

    var container: Container? = nil
    var viewController: UIViewController? = nil

    func navigateToAddEmployeeScreen() {
        var employeeViewController = container?.resolve(EmployeeViewProtocol.self)
        employeeViewController?.presenter?.screenMode = ScreenMode.AddEmployee
        employeeViewController?.presenter?.editEmployee = nil
        viewController?.navigationController?.pushViewController(employeeViewController as! UIViewController, animated: true)
    }

    func navigateToEditEmployeeScreen(employee: Employee) {
        var employeeViewController = container?.resolve(EmployeeViewProtocol.self)
        employeeViewController?.presenter?.screenMode = ScreenMode.EditEmployee
        employeeViewController?.presenter?.editEmployee = employee
        viewController?.navigationController?.pushViewController(employeeViewController as! UIViewController, animated: true)
    }
}
