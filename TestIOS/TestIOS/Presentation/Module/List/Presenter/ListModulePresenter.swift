//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import RxSwift

class ListModulePresenter: BasePresenter, ListPresenterProtocol {

    var view: ListViewProtocol? {
        didSet {
            baseView = view
        }
    }
    var router: ListWireframeProtocol? = nil
    var fetchEmployeeListInteractor: FetchListEmployeeInteractor? = nil
    var sortEmployeesInteractor: SortEmployeesInteractor? = nil
    var removeWorkerInteractor: RemoveWorkerInteractor? = nil
    var removeAccountantInteractor: RemoveAccountantInteractor? = nil
    var removeLeaderInteractor: RemoveLeaderInteractor? = nil
    var saveWorkerInteractor: SaveWorkerInteractor? = nil
    var saveLeaderInteractor: SaveLeaderInteractor? = nil
    var saveAccountantInteractor: SaveAccountantInteractor? = nil
    var accountants: Array<Accountant>? = nil
    var leaders: Array<Leader>? = nil
    var workers: Array<Worker>? = nil

    override func viewDidAppear(view: BaseViewProtocol) {
        super.viewDidAppear(view: view)
        if !self.view!.isWasRunEditMode() {
            self.view?.showEditBtn()
        } else {
            self.view?.showDoneBtn()
        }
        fetchEmployeeList()
    }

    func onClickedBtnEdit() {
        view?.hideEditBtn()
        view?.showDoneBtn()
        view?.setEnableEditMode()
    }

    func onClickedBtnDone() {
        view?.hideDoneBtn()
        view?.showEditBtn()
        view?.setDisableEditMode()
    }

    func onClickedBtnAdd() {
        router?.navigateToAddEmployeeScreen()
    }

    func onClickedBtnSort() {
        sortEmployeesInteractor?.execute(onSubscribe: doOnSubscribeAnyInteractor, onNext: { tuple in
            self.workers = tuple.workers
            self.leaders = tuple.leaders
            self.accountants = tuple.accountants
            if self.isEmptyEmployeeArrays() {
                self.view?.showEmptyView()
            } else {
                self.view?.showEmployees(accountants: tuple.accountants, leaders: tuple.leaders, workers: tuple.workers)
            }
        },
                onError: onErrorAnyInteractor,
                onDisposed: doOnDisposedAnyInteractor,
                param: (accountants: accountants!, leaders: leaders!, workers: workers!))
    }

    private func fetchEmployeeList() {
        fetchEmployeeListInteractor?.execute(onSubscribe: {
            self.view?.hideList()
            self.view?.hideEmptyView()
            self.view?.showPending()
        }, onNext: { tuple in
            self.workers = tuple.workers
            self.leaders = tuple.leaders
            self.accountants = tuple.accountants
            if self.isEmptyEmployeeArrays() {
                self.view?.showEmptyView()
            } else {
                self.view?.showEmployees(accountants: tuple.accountants, leaders: tuple.leaders, workers: tuple.workers)
            }
        },
                onDisposed: doOnDisposedAnyInteractor)
    }

    private func doOnDisposedAnyInteractor() {
        self.view?.hidePending()
    }

    func onDidSelectWorker(index: Int, worker: Worker) {
        router?.navigateToEditEmployeeScreen(employee: worker)
    }

    func onDidSelectLeader(index: Int, leader: Leader) {
        router?.navigateToEditEmployeeScreen(employee: leader)
    }

    func onDidSelectAccountant(index: Int, accountant: Accountant) {
        router?.navigateToEditEmployeeScreen(employee: accountant)
    }

    func onClickRemoveWorker(indexPath: IndexPath) {
        let employee = self.workers![indexPath.row]
        view?.showAlertDialogQuestionWithTitle(R.string.localizable.delete(),
                message: "\(R.string.localizable.questionDeleteWorker()) \(employee.name!) \(employee.secondName!) \(employee.patronymic!)?",
                onClickPositiveButtonCallback: {
                    let removeWorker = self.workers![indexPath.row]
                    self.removeWorkerInteractor?.execute(onSubscribe: self.doOnSubscribeAnyInteractor, onCompleted: {
                        self.workers?.remove(at: indexPath.row)
                        self.view?.deleteWorkerWithIndexPath(indexPath: indexPath)
                        if self.isEmptyEmployeeArrays() {
                            self.view?.showEmptyView()
                        }
                    },
                            onDisposed: self.doOnDisposedAnyInteractor,
                            param: removeWorker)
                })
    }

    func onClickRemoveLeader(indexPath: IndexPath) {
        let employee = self.leaders![indexPath.row]
        view?.showAlertDialogQuestionWithTitle(R.string.localizable.delete(),
                message: "\(R.string.localizable.questionDeleteLeader()) \(employee.name!) \(employee.secondName!) \(employee.patronymic!)?",
                onClickPositiveButtonCallback: {
                    let removeLeader = self.leaders![indexPath.row]
                    self.removeLeaderInteractor?.execute(onSubscribe: self.doOnSubscribeAnyInteractor, onCompleted: {
                        self.leaders?.remove(at: indexPath.row)
                        self.view?.deleteLeaderWithIndexPath(indexPath: indexPath)
                        if self.isEmptyEmployeeArrays() {
                            self.view?.showEmptyView()
                        }
                    },
                            onDisposed: self.doOnDisposedAnyInteractor,
                            param: removeLeader)
                })
    }

    func onClickRemoveAccountant(indexPath: IndexPath) {
        let employee = self.leaders![indexPath.row]
        view?.showAlertDialogQuestionWithTitle(R.string.localizable.delete(),
                message: "\(R.string.localizable.questionDeleteAccountant()) \(employee.name!) \(employee.secondName!) \(employee.patronymic!)?",
                onClickPositiveButtonCallback:
                {
                    let removeAccountant = self.accountants![indexPath.row]
                    self.removeAccountantInteractor?.execute(onSubscribe: self.doOnSubscribeAnyInteractor, onCompleted: {
                        self.accountants?.remove(at: indexPath.row)
                        self.view?.deleteAccountantWithIndexPath(indexPath: indexPath)
                        if self.isEmptyEmployeeArrays() {
                            self.view?.showEmptyView()
                        }
                    },
                            onDisposed: self.doOnDisposedAnyInteractor,
                            param: removeAccountant)
                })
    }

    private func doOnSubscribeAnyInteractor() {
        self.view?.showPending()
    }

    private func onErrorAnyInteractor(error: Swift.Error) {
        self.view?.hidePending()
    }

    private func isEmptyEmployeeArrays() -> Bool {
        return isEmptyWorkerArray() && isEmptyLeaderArray() && isEmptyAccountantArray()
    }

    private func isEmptyWorkerArray() -> Bool {
        return workers == nil ? true : workers!.isEmpty
    }

    private func isEmptyLeaderArray() -> Bool {
        return leaders == nil ? true : leaders!.isEmpty
    }

    private func isEmptyAccountantArray() -> Bool {
        return accountants == nil ? true : accountants!.isEmpty
    }

    func onMoveLeader(to: Int, from: Int) {
        var updateEmployees = Array<Leader>()
        var tempEmployee = leaders!.remove(at: from)
        leaders!.insert(tempEmployee, at: to)

        //вычисляем область сотрудников чьи позиции изменились, чтобы не перезагружать весь список
        if from > to {
            for index in (to + 1)...from {
                tempEmployee = leaders![index]
                tempEmployee.position = tempEmployee.position + 1
                updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)
            }
        } else if to > from {
            for index in from...to - 1 {
                tempEmployee = leaders![index]
                tempEmployee.position = tempEmployee.position - 1
                updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)
            }
        }

        tempEmployee = leaders![to]
        tempEmployee.position = to + 1
        updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)

        saveLeaderInteractor?.execute(onSubscribe: doOnSubscribeAnyInteractor,
                onError: onErrorAnyInteractor,
                onCompleted: {
                    self.leaders = self.leaders!.map { leader -> Leader in
                        Leader(value: leader)
                    }
                },
                onDisposed: doOnDisposedAnyInteractor,
                param: updateEmployees)

#if DEBUG
        leaders!.forEach { leader in
            print("onMoveLeader.leader with name = \(leader.name!) \(leader.secondName!) \(leader.patronymic!), position = \(leader.position)")
        }
#endif
    }

    func onMoveWorker(to: Int, from: Int) {

        var updateEmployees = Array<Worker>()
        var tempEmployee = workers!.remove(at: from)
        workers!.insert(tempEmployee, at: to)

        //вычисляем область сотрудников чьи позиции изменились, чтобы не перезагружать весь список
        if from > to {
            for index in (to + 1)...from {
                tempEmployee = workers![index]
                tempEmployee.position = tempEmployee.position + 1
                updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)
            }
        } else if to > from {
            for index in from...to - 1 {
                tempEmployee = workers![index]
                tempEmployee.position = tempEmployee.position - 1
                updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)
            }
        }

        tempEmployee = workers![to]
        tempEmployee.position = to + 1
        updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)

        saveWorkerInteractor?.execute(onSubscribe: doOnSubscribeAnyInteractor,
                onError: onErrorAnyInteractor,
                onCompleted: {
                    self.workers = self.workers!.map { worker -> Worker in
                        Worker(value: worker)
                    }
                },
                onDisposed: doOnDisposedAnyInteractor,
                param: updateEmployees)

#if DEBUG
        workers!.forEach { worker in
            print("onMoveWorker.worker with name = \(worker.name!) \(worker.secondName!) \(worker.patronymic!), position = \(worker.position)")
        }
#endif

    }

    func onMoveAccountant(to: Int, from: Int) {
        var updateEmployees = Array<Accountant>()
        var tempEmployee = accountants!.remove(at: from)
        accountants!.insert(tempEmployee, at: to)

        //вычисляем область сотрудников чьи позиции изменились, чтобы не перезагружать весь список
        if from > to {
            for index in (to + 1)...from {
                tempEmployee = accountants![index]
                tempEmployee.position = tempEmployee.position + 1
                updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)
            }
        } else if to > from {
            for index in from...to - 1 {
                tempEmployee = accountants![index]
                tempEmployee.position = tempEmployee.position - 1
                updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)
            }
        }

        tempEmployee = accountants![to]
        tempEmployee.position = to + 1
        updateEmployees.insert(tempEmployee, at: updateEmployees.endIndex)

        saveAccountantInteractor?.execute(onSubscribe: doOnSubscribeAnyInteractor,
                onError: onErrorAnyInteractor,
                onCompleted: {
                    self.accountants = self.accountants!.map { accountant -> Accountant in
                        Accountant(value: accountant)
                    }
                },
                onDisposed: doOnDisposedAnyInteractor,
                param: updateEmployees)

#if DEBUG
        accountants!.forEach { accountant in
            print("onMoveAccountant.accountant with name = \(accountant.name!) \(accountant.secondName!) \(accountant.patronymic!), position = \(accountant.position)")
        }
#endif
    }

}
