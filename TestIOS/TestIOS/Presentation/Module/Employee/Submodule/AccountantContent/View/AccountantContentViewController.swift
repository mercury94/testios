//
//  AccountantContentViewController.swift
//  TestIOS
//
//  Created by Nikita on 04.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import DatePickerDialog

class AccountantContentViewController: BaseViewController, AccountantContentViewProtocol, UITextFieldDelegate {
    var presenter: AccountantContentPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }
    @IBOutlet weak var numberWorkplaceTextField: UITextField!
    @IBOutlet weak var lunchBeginTime: UILabel!
    @IBOutlet weak var lunchEndTime: UILabel!
    @IBOutlet weak var typeAccountantSegmentControl: UISegmentedControl!
    @IBOutlet weak var typeAccountantLabel: UILabel!
    @IBOutlet weak var lunchTimeLabel: UILabel!
    private let datePickerDialog = DatePickerDialog()
    private let timeFormatter = DateFormatter()

    override func initViews() {
        super.initViews()
        timeFormatter.dateFormat = "hh : mm"
        lunchBeginTime.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickedLabelLunchBeginTime)))
        lunchEndTime.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickedLabelLunchEndTime)))

        numberWorkplaceTextField.placeholder = R.string.localizable.numberWorkplace()
        numberWorkplaceTextField.returnKeyType = .done
        numberWorkplaceTextField.delegate = self
        
        typeAccountantLabel.text = R.string.localizable.typeAccountant()
        
        typeAccountantSegmentControl.setTitle(R.string.localizable.typeAccountantPayroll(), forSegmentAt: 0)
        typeAccountantSegmentControl.setTitle(R.string.localizable.typeAccountantMaterials(), forSegmentAt: 1)

        lunchTimeLabel.text = "\(R.string.localizable.lunchTime()):"

    }

    func clickedLabelLunchBeginTime(sender: UITapGestureRecognizer) {
        presenter?.clickedBtnLunchBeginTime()
    }

    func clickedLabelLunchEndTime(sender: UITapGestureRecognizer) {
        presenter?.clickedBtnLunchEndTime()
    }

    func showTimePickerLunchBegin() {
        datePickerDialog.showDefaultTimePickerDialog(R.string.localizable.lunchBeginTime()) {
            (timeDate) -> Void in
            if timeDate != nil {
                self.lunchBeginTime.text = self.timeFormatter.string(from: timeDate!)
            }
        }
    }

    func showTimePickerLunchEnd() {
        datePickerDialog.showDefaultTimePickerDialog(R.string.localizable.lunchEndTime()) {
            (timeDate) -> Void in
            if timeDate != nil {
                self.lunchEndTime.text = self.timeFormatter.string(from: timeDate!)
            }
        }
    }

    func getNumberWorkplace() -> String? {
        return numberWorkplaceTextField.text
    }

    func getLunchBeginTime() -> String? {
        return lunchBeginTime.text
    }

    func getLunchEndTime() -> String? {
        return lunchEndTime.text
    }

    func getAccountantType() -> String? {
        var type: String? = nil
        if typeAccountantSegmentControl.selectedSegmentIndex == AccountantType.payroll.0 {
            type = AccountantType.payroll.1
        } else if typeAccountantSegmentControl.selectedSegmentIndex == AccountantType.materials.0 {
            type = AccountantType.materials.1
        }
        return type
    }

    func showData(accountant: Accountant) {
        numberWorkplaceTextField.text = accountant.numberWorkplace
        lunchBeginTime.text = accountant.lunchBeginTime
        lunchEndTime.text = accountant.lunchEndTime
        if accountant.typeAccountant == AccountantType.payroll.1 {
            typeAccountantSegmentControl.selectedSegmentIndex = 0
        } else if accountant.typeAccountant == AccountantType.materials.1 {
            typeAccountantSegmentControl.selectedSegmentIndex = 1
        }
    }

    func cleanFields() {
        numberWorkplaceTextField.text = ""
        lunchBeginTime.text = "00 : 00"
        lunchEndTime.text = "00 : 00"
        typeAccountantSegmentControl.selectedSegmentIndex = 0
    }

    //region UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == numberWorkplaceTextField {
            view.endEditing(true)
        }
        return true
    }

    //endregion UITextFieldDelegate
}
