//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

struct AccountantType {
    static let payroll = (0, "payroll")
    static let materials = (1, "materials")
}

protocol AccountantContentViewProtocol: BaseViewProtocol {

    var presenter: AccountantContentPresenterProtocol? { get set }

    func showTimePickerLunchBegin()

    func showTimePickerLunchEnd()

    func getNumberWorkplace() -> String?

    func getLunchBeginTime() -> String?

    func getLunchEndTime() -> String?

    func getAccountantType() -> String?

    func showData(accountant: Accountant)

    func cleanFields()

}

protocol AccountantContentPresenterProtocol: BasePresenterProtocol {

    var viewAccountantContent: AccountantContentViewProtocol? { get set }

    var router: EmployeeWireframeProtocol? { get set }

    var isFirstAccountantContentViewAppear: Bool { get set }

    func clickedBtnLunchBeginTime()

    func clickedBtnLunchEndTime()

}