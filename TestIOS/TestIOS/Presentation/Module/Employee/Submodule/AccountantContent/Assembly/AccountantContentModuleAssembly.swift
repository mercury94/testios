//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class AccountantContentModuleAssembly: Assembly {

    func assemble(container: Container) {

        container.register(AccountantContentViewProtocol.self) { r in
                    let viewController = AccountantContentViewController(nibName: "AccountantContentViewController", bundle: nil)
                    var presenter = r.resolve(EmployeePresenterProtocol.self) as! AccountantContentPresenterProtocol
                    viewController.presenter = presenter
                    presenter.viewAccountantContent = viewController
                    return viewController
                }
                .inObjectScope(.container)


    }

}
