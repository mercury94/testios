//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//


protocol LeaderContentViewProtocol: BaseViewProtocol {

    var presenter: LeaderContentPresenterProtocol? { get set }

    func showTimePickerBusinessHoursBegin()

    func showTimePickerBusinessHoursEnd()

    func getBusinessHoursBeginTime() -> String?

    func getBusinessHoursEndTime() -> String?

    func showData(leader: Leader)

    func cleanFields()

}

protocol LeaderContentPresenterProtocol: BasePresenterProtocol {

    var viewLeaderContent: LeaderContentViewProtocol? { get set }

    var router: EmployeeWireframeProtocol? { get set }

    var isFirstLeaderContentViewAppear: Bool { get set }

    func clickedBtnBusinessHoursBeginTime()

    func clickedBtnBusinessHoursEndTime()

}