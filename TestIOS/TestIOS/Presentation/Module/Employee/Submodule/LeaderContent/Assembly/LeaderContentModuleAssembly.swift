//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class LeaderContentModuleAssembly: Assembly {


    func assemble(container: Container) {

        container.register(LeaderContentViewProtocol.self) { r in
                    let viewController = LeaderContentViewController(nibName: "LeaderContentViewController", bundle: nil)
                    var presenter = r.resolve(EmployeePresenterProtocol.self) as! LeaderContentPresenterProtocol
                    viewController.presenter = presenter
                    presenter.viewLeaderContent = viewController
                    return viewController
                }
                .inObjectScope(.container)


    }
}
