//
//  LeaderContentViewController.swift
//  TestIOS
//
//  Created by Nikita on 04.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import DatePickerDialog

class LeaderContentViewController: BaseViewController, LeaderContentViewProtocol {

    var presenter: LeaderContentPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    @IBOutlet weak var businessHourseLabel: UILabel!
    @IBOutlet weak var businessHoursBeginTimeTextField: UILabel!
    @IBOutlet weak var businessHoursEndTimeTextField: UILabel!

    private let datePickerDialog = DatePickerDialog()
    private let timeFormatter = DateFormatter()

    override func initViews() {
        super.initViews()
        businessHourseLabel.text = "\(R.string.localizable.businessHoursTime()):"
        timeFormatter.dateFormat = "hh : mm"
        businessHoursBeginTimeTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickedLabelBusinessHoursBeginTime)))
        businessHoursEndTimeTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickedLabelBusinessHoursEndTime)))
    }

    func clickedLabelBusinessHoursBeginTime(sender: UITapGestureRecognizer) {
        presenter?.clickedBtnBusinessHoursBeginTime()
    }

    func clickedLabelBusinessHoursEndTime(sender: UITapGestureRecognizer) {
        presenter?.clickedBtnBusinessHoursEndTime()
    }

    func showTimePickerBusinessHoursBegin() {
        datePickerDialog.showDefaultTimePickerDialog(R.string.localizable.businessHoursBeginTime()) {
            (timeDate) -> Void in
            if timeDate != nil {
                self.businessHoursBeginTimeTextField.text = self.timeFormatter.string(from: timeDate!)
            }
        }
    }

    func showTimePickerBusinessHoursEnd() {
        datePickerDialog.showDefaultTimePickerDialog(R.string.localizable.businessHoursEndTime()) {
            (timeDate) -> Void in
            if timeDate != nil {
                self.businessHoursEndTimeTextField.text = self.timeFormatter.string(from: timeDate!)
            }
        }
    }

    func getBusinessHoursBeginTime() -> String? {
        return businessHoursBeginTimeTextField.text
    }

    func getBusinessHoursEndTime() -> String? {
        return businessHoursEndTimeTextField.text
    }


    func showData(leader: Leader) {
        businessHoursBeginTimeTextField.text = leader.businessHoursBeginTime
        businessHoursEndTimeTextField.text = leader.businessHoursEndTime
    }

    func cleanFields() {
        businessHoursBeginTimeTextField.text = "00 : 00"
        businessHoursEndTimeTextField.text = "00 : 00"
    }
}
