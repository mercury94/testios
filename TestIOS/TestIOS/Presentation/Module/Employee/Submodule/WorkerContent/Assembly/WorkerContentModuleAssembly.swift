//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject


class WorkerContentModuleAssembly: Assembly {

    func assemble(container: Container) {

        container.register(WorkerContentViewProtocol.self) { r in
                    let viewController = WorkerContentViewController(nibName: "WorkerContentViewController", bundle: nil)
                    var presenter = r.resolve(EmployeePresenterProtocol.self) as! WorkerPresenterProtocol
                    viewController.presenter = presenter
                    presenter.viewWorkContent = viewController
                    return viewController
                }
                .inObjectScope(.container)
    }
}
