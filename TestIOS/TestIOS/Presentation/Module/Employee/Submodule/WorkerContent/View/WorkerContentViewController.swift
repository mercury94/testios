//
//  WorkerContentViewController.swift
//  TestIOS
//
//  Created by Nikita on 04.10.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import DatePickerDialog

class WorkerContentViewController: BaseViewController, WorkerContentViewProtocol, UITextFieldDelegate {

    var presenter: WorkerPresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    @IBOutlet weak var numberWorkplaceTextField: UITextField!
    @IBOutlet weak var lunchBeginTime: UILabel!
    @IBOutlet weak var lunchEndTime: UILabel!
    @IBOutlet weak var lunchTimeLabel: UILabel!
    
    private let datePickerDialog = DatePickerDialog()
    private let timeFormatter = DateFormatter()

    override func initViews() {
        super.initViews()
        timeFormatter.dateFormat = "hh : mm"
        lunchBeginTime.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickedLabelLunchBeginTime)))
        lunchEndTime.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clickedLabelLunchEndTime)))

        numberWorkplaceTextField.placeholder = R.string.localizable.numberWorkplace()
        numberWorkplaceTextField.returnKeyType = .done
        numberWorkplaceTextField.delegate = self
        
        lunchTimeLabel.text = "\(R.string.localizable.lunchTime()):"
    }




    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.parent!.view.frame.origin.y == 0 {
                self.parent!.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.parent!.view.frame.origin.y != 0 {
                self.parent!.view.frame.origin.y += keyboardSize.height
            }
        }
    }

    func clickedLabelLunchBeginTime(sender: UITapGestureRecognizer) {
        presenter?.clickedBtnLunchBeginTime()
    }

    func clickedLabelLunchEndTime(sender: UITapGestureRecognizer) {
        presenter?.clickedBtnLunchEndTime()
    }

    func showTimePickerLunchBegin() {
        datePickerDialog.showDefaultTimePickerDialog("Начало обеда") {
            (timeDate) -> Void in
            if timeDate != nil {
                self.lunchBeginTime.text = self.timeFormatter.string(from: timeDate!)
            }
        }
    }

    func showTimePickerLunchEnd() {
        datePickerDialog.showDefaultTimePickerDialog("Окончание обеда") {
            (timeDate) -> Void in
            if timeDate != nil {
                self.lunchEndTime.text = self.timeFormatter.string(from: timeDate!)
            }
        }
    }

    func getNumberWorkplace() -> String? {
        return numberWorkplaceTextField.text
    }

    func getLunchBeginTime() -> String? {
        return lunchBeginTime.text
    }

    func getLunchEndTime() -> String? {
        return lunchEndTime.text
    }

    func showData(worker: Worker) {
        numberWorkplaceTextField.text = worker.numberWorkplace
        lunchBeginTime.text = worker.lunchBeginTime
        lunchEndTime.text = worker.lunchEndTime
    }

    func cleanFields() {
        numberWorkplaceTextField.text = ""
        lunchBeginTime.text = "00 : 00"
        lunchEndTime.text = "00 : 00"
    }

    //region UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == numberWorkplaceTextField {
            view.endEditing(true)
        }
        return true
    }

    //endregion UITextFieldDelegate

}
