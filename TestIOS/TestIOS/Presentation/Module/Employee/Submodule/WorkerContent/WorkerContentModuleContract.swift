//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//


protocol WorkerContentViewProtocol: BaseViewProtocol {

    var presenter: WorkerPresenterProtocol? { get set }

    func showTimePickerLunchBegin()

    func showTimePickerLunchEnd()

    func getNumberWorkplace() -> String?

    func getLunchBeginTime() -> String?

    func getLunchEndTime() -> String?

    func showData(worker: Worker)

    func cleanFields()
}

protocol WorkerPresenterProtocol: BasePresenterProtocol {

    var viewWorkContent: WorkerContentViewProtocol? { get set }

    var router: EmployeeWireframeProtocol? { get set }

    var isFirstWorkerContentViewAppear: Bool { get set }

    func clickedBtnLunchBeginTime()

    func clickedBtnLunchEndTime()

}