//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject
import RxSwift

class EmployeeModuleAssembly: Assembly {

    func assemble(container: Container) {

        container.register(EmployeeWireframeProtocol.self) { _ in
            let router = EmployeeModuleRouter()
            router.container = container
            return router
        }

        container.register(SaveWorkerInteractor.self) { r in
            return SaveWorkerInteractor(workerRepository: r.resolve(WorkerRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(SaveLeaderInteractor.self) { r in
            return SaveLeaderInteractor(leaderRepository: r.resolve(LeaderRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(SaveAccountantInteractor.self) { r in
            return SaveAccountantInteractor(accountantRepository: r.resolve(AccountantRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(RemoveWorkerInteractor.self) { r in
            return RemoveWorkerInteractor(workerRepository: r.resolve(WorkerRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(RemoveLeaderInteractor.self) { r in
            return RemoveLeaderInteractor(leaderRepository: r.resolve(LeaderRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(RemoveAccountantInteractor.self) { r in
            return RemoveAccountantInteractor(accountantRepository: r.resolve(AccountantRepositoryProtocol.self)!,
                    workerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!,
                    observerScheduler: r.resolve(ImmediateSchedulerType.self, name: RxSchedulerAssembly.mainScheduler)!)
        }

        container.register(EmployeePresenterProtocol.self) { r in
                    let presenter = EmployeeModulePresenter()
                    presenter.saveWorkerInteractor = r.resolve(SaveWorkerInteractor.self)!
                    presenter.saveLeaderInteractor = r.resolve(SaveLeaderInteractor.self)!
                    presenter.saveAccountantInteractor = r.resolve(SaveAccountantInteractor.self)!
                    presenter.removeWorkerInteractor = r.resolve(RemoveWorkerInteractor.self)!
                    presenter.removeLeaderInteractor = r.resolve(RemoveLeaderInteractor.self)!
                    presenter.removeAccountantInteractor = r.resolve(RemoveAccountantInteractor.self)!
                    return presenter
                }
                .inObjectScope(.container)

        container.register(EmployeeViewProtocol.self) { r in
            let viewController = EmployeeViewController(nibName: "EmployeeViewController", bundle: nil)
            var presenter = r.resolve(EmployeePresenterProtocol.self)
            var router = r.resolve(EmployeeWireframeProtocol.self)
            viewController.presenter = presenter
            presenter?.view = viewController
            presenter?.router = router
            router?.viewController = viewController as UIViewController
            return viewController
        }

    }

}
