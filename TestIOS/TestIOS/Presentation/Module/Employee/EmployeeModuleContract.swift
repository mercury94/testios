//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit

enum ScreenMode {
    case AddEmployee
    case EditEmployee
}

enum EmployeeType: Int {
    case Leader = 0
    case Accountant
    case Worker
}

protocol EmployeeViewProtocol: BaseViewProtocol {

    var presenter: EmployeePresenterProtocol? { get set }

    func getName() -> String?

    func getSecondName() -> String?

    func getPatronymic() -> String?

    func getSalary() -> String?

    func showBaseData(employee: Employee)

    func cleanFields()

    func selectTypeEmployee(type: EmployeeType)
}

protocol EmployeePresenterProtocol: BasePresenterProtocol {

    var view: EmployeeViewProtocol? { get set }

    var router: EmployeeWireframeProtocol? { get set }

    var screenMode: ScreenMode? { get set }

    var editEmployee: Employee? { get set }

    var saveWorkerInteractor: SaveWorkerInteractor? { get set }

    var saveLeaderInteractor: SaveLeaderInteractor? { get set }

    var saveAccountantInteractor: SaveAccountantInteractor? { get set }

    var removeWorkerInteractor: RemoveWorkerInteractor? { get set }

    var removeAccountantInteractor: RemoveAccountantInteractor? { get set }

    var removeLeaderInteractor: RemoveLeaderInteractor? { get set }

    func clickedBtnSave()

    func selectedEmployeeTypeLeader()

    func selectedEmployeeTypeAccountant()

    func selectedEmployeeTypeWorker()

}

protocol EmployeeWireframeProtocol: BaseWireframeProtocol {

    var employeeTypeContentView: UIView? { get set }

    func showEmployeeTypeLeaderContent()

    func showEmployeeTypeAccountantContent()

    func showEmployeeTypeWorkerContent()

    func closeScreen()

}