//
//  EmployeeViewController.swift
//  TestIOS
//
//  Created by Nikita on 03.10.17.
//  Copyright © 2017 Nikita. All rigWhts reserved.
//

import UIKit

class EmployeeViewController: BaseViewController, EmployeeViewProtocol, UITextFieldDelegate {

    var presenter: EmployeePresenterProtocol? {
        didSet {
            basePresenter = presenter
        }
    }

    @IBOutlet weak var scrollVIew: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var secondNameTextField: UITextField!
    @IBOutlet weak var patronymicTextField: UITextField!
    @IBOutlet weak var salaryTextField: UITextField!
    @IBOutlet weak var employeeTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var employeeTypeContentView: UIView! {
        didSet {
            presenter?.router?.employeeTypeContentView = employeeTypeContentView
        }
    }
    @IBOutlet weak var postLabel: UILabel!
    
    override func initViews() {
        super.initViews()
        
        nameTextField.placeholder = R.string.localizable.name()
        nameTextField.returnKeyType = .next
        nameTextField.delegate = self

        secondNameTextField.placeholder = R.string.localizable.secondName()
        secondNameTextField.returnKeyType = .next
        secondNameTextField.delegate = self

        patronymicTextField.placeholder = R.string.localizable.patronymic()
        patronymicTextField.returnKeyType = .next
        patronymicTextField.delegate = self

        salaryTextField.placeholder = R.string.localizable.salary()
        salaryTextField.returnKeyType = .done
        salaryTextField.delegate = self
        salaryTextField.keyboardType = .numbersAndPunctuation
        
        postLabel.text = R.string.localizable.post()
        employeeTypeSegmentedControl.setTitle(R.string.localizable.leadership(), forSegmentAt: 0)
        employeeTypeSegmentedControl.setTitle(R.string.localizable.accountant(), forSegmentAt: 1)
        employeeTypeSegmentedControl.setTitle(R.string.localizable.worker(), forSegmentAt: 2)

    }


    override func setTitle(title: String) {
        self.title = title
    }

    override func createRightBarButtonItem() -> UIBarButtonItem? {
        return UIBarButtonItem(title: R.string.localizable.saveEmployee(),
                style: UIBarButtonItemStyle.done,
                target: self,
                action: #selector(self.clickedBtnSave(sender:)))
    }

    @objc private func clickedBtnSave(sender: UIButton) {
        presenter?.clickedBtnSave()
    }

    func getName() -> String? {
        return nameTextField.text
    }

    func getSecondName() -> String? {
        return secondNameTextField.text
    }

    func getPatronymic() -> String? {
        return patronymicTextField.text
    }

    func getSalary() -> String? {
        return salaryTextField.text
    }

    @IBAction func didChangedTypeSegControl(_ sender: UISegmentedControl) {
        let index = employeeTypeSegmentedControl.selectedSegmentIndex
        if index == EmployeeType.Leader.rawValue {
            presenter?.selectedEmployeeTypeLeader()
        } else if index == EmployeeType.Accountant.rawValue {
            presenter?.selectedEmployeeTypeAccountant()
        } else if index == EmployeeType.Worker.rawValue {
            presenter?.selectedEmployeeTypeWorker()
        }
    }

    func showBaseData(employee: Employee) {
        nameTextField.text = employee.name
        secondNameTextField.text = employee.secondName
        patronymicTextField.text = employee.patronymic
        salaryTextField.text = employee.salary
    }

    func cleanFields() {
        nameTextField.text = ""
        secondNameTextField.text = ""
        patronymicTextField.text = ""
        salaryTextField.text = ""
    }

    func selectTypeEmployee(type: EmployeeType) {
        employeeTypeSegmentedControl.selectedSegmentIndex = type.rawValue
    }

    //region UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            secondNameTextField.becomeFirstResponder()
        } else if textField == secondNameTextField {
            patronymicTextField.becomeFirstResponder()
        } else if textField == patronymicTextField {
            salaryTextField.becomeFirstResponder()
        } else if textField == salaryTextField {
            view.endEditing(true)
        }
        return true
    }

    //endregion UITextFieldDelegate

}
