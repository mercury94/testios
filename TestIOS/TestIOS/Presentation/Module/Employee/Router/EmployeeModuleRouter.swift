//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject
import UIKit

class EmployeeModuleRouter: EmployeeWireframeProtocol {
    var container: Container? = nil
    var viewController: UIViewController? = nil
    var employeeTypeContentView: UIView? = nil
    var currentViewControllerInContainer: UIViewController? = nil

    func showEmployeeTypeLeaderContent() {
        let vc = container!.resolve(LeaderContentViewProtocol.self) as! UIViewController
        displayViewController(viewController: vc)
    }

    func showEmployeeTypeAccountantContent() {
        let vc = container!.resolve(AccountantContentViewProtocol.self) as! UIViewController
        displayViewController(viewController: vc)
    }

    func showEmployeeTypeWorkerContent() {
        let vc = container!.resolve(WorkerContentViewProtocol.self) as! UIViewController
        displayViewController(viewController: vc)
    }

    private func displayViewController(viewController: UIViewController) {
        if (currentViewControllerInContainer != nil) {
            hideCurrentViewControllerInContainer()
        }

        self.viewController?.addChildViewController(viewController)
        viewController.view?.frame = employeeTypeContentView!.bounds
        employeeTypeContentView?.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self.viewController)
        currentViewControllerInContainer = viewController
    }

    private func hideCurrentViewControllerInContainer() {
        currentViewControllerInContainer?.willMove(toParentViewController: nil)
        currentViewControllerInContainer?.view.removeFromSuperview()
        currentViewControllerInContainer?.removeFromParentViewController()
        currentViewControllerInContainer = nil
    }

    func closeScreen() {
        viewController?.navigationController?.popViewController(animated: true)
        viewController?.dismiss(animated: true, completion: nil)
    }
}
