//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

class EmployeeModulePresenter: BasePresenter,
        EmployeePresenterProtocol,
        LeaderContentPresenterProtocol,
        WorkerPresenterProtocol,
        AccountantContentPresenterProtocol {

    private typealias RemoveCallback = () -> Void
    private var currentEmployeeType: EmployeeType?
    var view: EmployeeViewProtocol? {
        didSet {
            baseView = view
        }
    }
    var router: EmployeeWireframeProtocol? = nil
    var saveWorkerInteractor: SaveWorkerInteractor? = nil
    var saveLeaderInteractor: SaveLeaderInteractor? = nil
    var saveAccountantInteractor: SaveAccountantInteractor? = nil
    var removeWorkerInteractor: RemoveWorkerInteractor? = nil
    var removeAccountantInteractor: RemoveAccountantInteractor? = nil
    var removeLeaderInteractor: RemoveLeaderInteractor? = nil
    var viewLeaderContent: LeaderContentViewProtocol? = nil
    var viewWorkContent: WorkerContentViewProtocol? = nil
    var viewAccountantContent: AccountantContentViewProtocol? = nil
    var isFirstWorkerContentViewAppear: Bool = true
    var isFirstLeaderContentViewAppear: Bool = true
    var isFirstAccountantContentViewAppear: Bool = true
    var screenMode: ScreenMode? = ScreenMode.AddEmployee
    var editEmployee: Employee? = nil

    override func viewDidAppear(view: BaseViewProtocol) {
        if (view is EmployeeViewProtocol) {
            configureBaseContentView()
        } else if view is WorkerContentViewProtocol {
            configureWorkerContentView()
        } else if view is LeaderContentViewProtocol {
            configureLeaderContentView()
        } else if view is AccountantContentViewProtocol {
            configureAccountantContentView()
        }
    }

    private func configureBaseContentView() {
        isFirstWorkerContentViewAppear = true
        isFirstLeaderContentViewAppear = true
        isFirstAccountantContentViewAppear = true
        if screenMode == ScreenMode.AddEmployee {
            view?.setTitle(title: R.string.localizable.addEmployee())
            configureScreenForAdd()
        } else if screenMode == ScreenMode.EditEmployee {
            view?.setTitle(title: R.string.localizable.editEmployee())
            configureScreenForEdit()
        }
    }

    private func configureWorkerContentView() {
        if (isFirstWorkerContentViewAppear) {
            isFirstWorkerContentViewAppear = false
            if screenMode == ScreenMode.AddEmployee {
                viewWorkContent?.cleanFields()
            } else if screenMode == ScreenMode.EditEmployee {
                if editEmployee is Worker {
                    viewWorkContent?.showData(worker: editEmployee as! Worker)
                } else {
                    viewWorkContent?.cleanFields()
                }
            }
        }
    }

    private func configureLeaderContentView() {
        if (isFirstLeaderContentViewAppear) {
            isFirstLeaderContentViewAppear = false
            if screenMode == ScreenMode.AddEmployee {
                viewLeaderContent?.cleanFields()
            } else if screenMode == ScreenMode.EditEmployee {
                if editEmployee is Leader {
                    viewLeaderContent?.showData(leader: editEmployee as! Leader)
                } else {
                    viewLeaderContent?.cleanFields()
                }
            }
        }
    }

    private func configureAccountantContentView() {
        if (isFirstAccountantContentViewAppear) {
            isFirstAccountantContentViewAppear = false
            if screenMode == ScreenMode.AddEmployee {
                viewAccountantContent?.cleanFields()
            } else if screenMode == ScreenMode.EditEmployee {
                if editEmployee is Accountant {
                    viewAccountantContent?.showData(accountant: editEmployee as! Accountant)
                } else {
                    viewAccountantContent?.cleanFields()
                }
            }
        }
    }

    private func configureScreenForEdit() {
        view?.showBaseData(employee: editEmployee!)
        if editEmployee is Worker {
            view?.selectTypeEmployee(type: EmployeeType.Worker)
            router?.showEmployeeTypeWorkerContent()
            currentEmployeeType = EmployeeType.Worker
        } else if editEmployee is Leader {
            view?.selectTypeEmployee(type: EmployeeType.Leader)
            router?.showEmployeeTypeLeaderContent()
            currentEmployeeType = EmployeeType.Leader
        } else if editEmployee is Accountant {
            view?.selectTypeEmployee(type: EmployeeType.Accountant)
            router?.showEmployeeTypeAccountantContent()
            currentEmployeeType = EmployeeType.Accountant
        }
    }

    private func configureScreenForAdd() {
        view?.cleanFields()
        router?.showEmployeeTypeLeaderContent()
        view?.selectTypeEmployee(type: EmployeeType.Leader)
        currentEmployeeType = EmployeeType.Leader
    }

    private func checkFields() -> Bool {

        if String.isNullOrEmpty(string: view?.getName()) {
            view?.showMessage(message: R.string.localizable.emptyNameFieldError())
            return false
        }

        if String.isNullOrEmpty(string: view?.getSecondName()) {
            view?.showMessage(message: R.string.localizable.emptySecondNameFieldError())
            return false
        }

        if String.isNullOrEmpty(string: view?.getPatronymic()) {
            view?.showMessage(message: R.string.localizable.emptyPatronymicFieldError())
            return false
        }

        if String.isNullOrEmpty(string: view?.getSalary()) {
            view?.showMessage(message: R.string.localizable.emptySalaryFieldError())
            return false
        }

        if currentEmployeeType == EmployeeType.Leader {
            return checkLeaderContentFields()
        } else if currentEmployeeType == EmployeeType.Accountant {
            return checkAccountantContentFields()
        } else if currentEmployeeType == EmployeeType.Worker {
            return checkWorkerContentFields()
        } else {
            view?.showMessage(message: R.string.localizable.selectPostEmployeeError())
            return false
        }

    }

    private func checkWorkerContentFields() -> Bool {

        if String.isNullOrEmpty(string: viewWorkContent?.getNumberWorkplace()) {
            view?.showMessage(message: R.string.localizable.emptyNumberWorkplaceFieldError())
            return false
        }

        if (String.isNullOrEmpty(string: viewWorkContent?.getLunchBeginTime())) {
            view?.showMessage(message: R.string.localizable.emptyLunchBeginFieldError())
            return false
        }

        if (String.isNullOrEmpty(string: viewWorkContent?.getLunchEndTime())) {
            view?.showMessage(message: R.string.localizable.emptyLunchEndFieldError())
            return false
        }

        return true
    }

    private func checkLeaderContentFields() -> Bool {

        if (String.isNullOrEmpty(string: viewLeaderContent?.getBusinessHoursBeginTime())) {
            view?.showMessage(message: R.string.localizable.emptyBusinessHoursBeginTimeFieldError())
            return false
        }

        if (String.isNullOrEmpty(string: viewLeaderContent?.getBusinessHoursEndTime())) {
            view?.showMessage(message: R.string.localizable.emptyBusinessHoursEndTimeFieldError())
            return false
        }

        return true

    }

    private func checkAccountantContentFields() -> Bool {

        if String.isNullOrEmpty(string: viewAccountantContent?.getNumberWorkplace()) {
            view?.showMessage(message: R.string.localizable.emptyNumberWorkplaceFieldError())
            return false
        }

        if (String.isNullOrEmpty(string: viewAccountantContent?.getLunchBeginTime())) {
            view?.showMessage(message: R.string.localizable.emptyLunchBeginFieldError())
            return false
        }

        if (String.isNullOrEmpty(string: viewAccountantContent?.getLunchEndTime())) {
            view?.showMessage(message: R.string.localizable.emptyLunchEndFieldError())
            return false
        }

        if (String.isNullOrEmpty(string: viewAccountantContent?.getAccountantType())) {
            view?.showMessage(message: R.string.localizable.selectTypetAccountantError())
            return false
        }

        return true

    }

    private func saveOrUpdateWorker() {
        let worker = Worker()
        worker.name = view?.getName()
        worker.secondName = view?.getSecondName()
        worker.patronymic = view?.getPatronymic()
        worker.salary = view?.getSalary()
        worker.numberWorkplace = viewWorkContent?.getNumberWorkplace()
        worker.lunchBeginTime = viewWorkContent?.getLunchBeginTime()
        worker.lunchEndTime = viewWorkContent?.getLunchEndTime()

        if screenMode == ScreenMode.AddEmployee {
            saveWorker(worker)
        } else if screenMode == ScreenMode.EditEmployee {
            updateWorker(worker)
        }
    }

    private func saveWorker(_ worker: Worker) {
        saveWorkerInteractor?.execute(onSubscribe: doOnSubscribeSaveOrRemoveEmployee,
                onError: onErrorSaveEmployee,
                onDisposed: onDisposedSaveEmployee,
                param: [worker])
    }

    private func updateWorker(_ worker: Worker) {
        if editEmployee is Worker {
            worker.id = editEmployee!.id
            saveWorker(worker)
        } else {
            //Удаляем выбранного сотрудника из соответствуюего списка
            removeEditEmployee() { _ in
                //и сохраняем нового сотрудника
                self.saveWorker(worker)
            }
        }
    }

    private func saveOrUpdateLeader() {
        let leader = Leader()
        leader.name = view?.getName()
        leader.secondName = view?.getSecondName()
        leader.patronymic = view?.getPatronymic()
        leader.salary = view?.getSalary()
        leader.businessHoursBeginTime = viewLeaderContent?.getBusinessHoursBeginTime()
        leader.businessHoursEndTime = viewLeaderContent?.getBusinessHoursEndTime()

        if screenMode == ScreenMode.AddEmployee {
            saveLeader(leader)
        } else if screenMode == ScreenMode.EditEmployee {
            updateLeader(leader)
        }
    }

    private func saveLeader(_ leader: Leader) {
        saveLeaderInteractor?.execute(onSubscribe: doOnSubscribeSaveOrRemoveEmployee,
                onError: onErrorSaveEmployee,
                onDisposed: onDisposedSaveEmployee,
                param: [leader])
    }

    private func updateLeader(_ leader: Leader) {
        if editEmployee is Leader {
            leader.id = editEmployee!.id
            saveLeader(leader)
        } else {
            //Удаляем выбранного сотрудника из соответствуюего списка
            removeEditEmployee() { _ in
                //и сохраняем нового руководителя
                self.saveLeader(leader)
            }
        }
    }

    private func saveOrUpdateAccountant() {
        let accountant = Accountant()
        accountant.name = view?.getName()
        accountant.secondName = view?.getSecondName()
        accountant.patronymic = view?.getPatronymic()
        accountant.salary = view?.getSalary()
        accountant.numberWorkplace = viewAccountantContent?.getNumberWorkplace()
        accountant.lunchBeginTime = viewAccountantContent?.getLunchBeginTime()
        accountant.lunchEndTime = viewAccountantContent?.getLunchEndTime()
        accountant.typeAccountant = viewAccountantContent?.getAccountantType()

        if screenMode == ScreenMode.AddEmployee {
            saveAccountant(accountant)
        } else if screenMode == ScreenMode.EditEmployee {
            updateAccountant(accountant)
        }
    }

    private func saveAccountant(_ accountant: Accountant) {
        saveAccountantInteractor?.execute(onSubscribe: doOnSubscribeSaveOrRemoveEmployee,
                onError: onErrorSaveEmployee,
                onDisposed: onDisposedSaveEmployee,
                param: [accountant])
    }

    private func updateAccountant(_ accountant: Accountant) {
        if editEmployee is Accountant {
            accountant.id = editEmployee!.id
            saveAccountant(accountant)
        } else {
            //Удаляем выбранного сотрудника из соответствуюего списка
            removeEditEmployee() { _ in
                //и сохраняем в нового бухгалтера
                self.saveAccountant(accountant)
            }
        }
    }

    private func doOnSubscribeSaveOrRemoveEmployee() {
        self.view?.showPending()
    }

    private func onErrorSaveEmployee(error: Swift.Error) {
        self.view?.hidePending()
    }

    private func onDisposedSaveEmployee() {
        self.view?.hidePending()
        router?.closeScreen()
    }

    private func removeEditEmployee(callback: @escaping RemoveCallback) {
        if editEmployee is Worker {
            removeWorkerInteractor?.execute(onSubscribe: doOnSubscribeSaveOrRemoveEmployee,
                    onCompleted: callback, param: editEmployee as! Worker!)
        } else if editEmployee is Leader {
            removeLeaderInteractor?.execute(onSubscribe: doOnSubscribeSaveOrRemoveEmployee,
                    onCompleted: callback, param: editEmployee as! Leader!)
        } else if editEmployee is Accountant {
            removeAccountantInteractor?.execute(onSubscribe: doOnSubscribeSaveOrRemoveEmployee,
                    onCompleted: callback, param: editEmployee as! Accountant!)
        }
    }

    func clickedBtnSave() {
        if checkFields() {
            if currentEmployeeType == EmployeeType.Worker {
                saveOrUpdateWorker()
            } else if currentEmployeeType == EmployeeType.Leader {
                saveOrUpdateLeader()
            } else if currentEmployeeType == EmployeeType.Accountant {
                saveOrUpdateAccountant()
            }
        }
    }

    func selectedEmployeeTypeLeader() {
        currentEmployeeType = EmployeeType.Leader
        router?.showEmployeeTypeLeaderContent()
    }

    func selectedEmployeeTypeAccountant() {
        currentEmployeeType = EmployeeType.Accountant
        router?.showEmployeeTypeAccountantContent()
    }

    func selectedEmployeeTypeWorker() {
        currentEmployeeType = EmployeeType.Worker
        router?.showEmployeeTypeWorkerContent()
    }

    func clickedBtnLunchBeginTime() {
        if currentEmployeeType == EmployeeType.Worker {
            viewWorkContent?.showTimePickerLunchBegin()
        } else if currentEmployeeType == EmployeeType.Accountant {
            viewAccountantContent?.showTimePickerLunchBegin()
        }
    }

    func clickedBtnLunchEndTime() {
        if currentEmployeeType == EmployeeType.Worker {
            viewWorkContent?.showTimePickerLunchEnd()
        } else if currentEmployeeType == EmployeeType.Accountant {
            viewAccountantContent?.showTimePickerLunchEnd()
        }
    }

    func clickedBtnBusinessHoursBeginTime() {
        if currentEmployeeType == EmployeeType.Leader {
            viewLeaderContent?.showTimePickerBusinessHoursBegin()
        }
    }

    func clickedBtnBusinessHoursEndTime() {
        if currentEmployeeType == EmployeeType.Leader {
            viewLeaderContent?.showTimePickerBusinessHoursEnd()
        }
    }

}
