//
//  AppDelegate.swift
//  TestIOS
//
//  Created by Nikita on 29.09.17.
//  Copyright © 2017 Nikita. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var assembler: Assembler?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureAssemblies()
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        let viewController = assembler!.resolver.resolve(SplashViewProtocol.self) as! UIViewController
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
        UINavigationBar.appearance().barStyle = .blackOpaque

        return true
    }

    func configureAssemblies() {
        assembler = Assembler([
            LocalRepositoryAssembly(),
            WebRepositoryAssembly(),
            RepositoryAssembly(),
            RxSchedulerAssembly(),
            SplashModuleAssembly(),
            MainNavigationModuleAssembly(),
            ListModuleAssembly(),
            GalleryModuleAssembly(),
            ServiceModuleAssembly(),
            EmployeeModuleAssembly(),
            LeaderContentModuleAssembly(),
            WorkerContentModuleAssembly(),
            AccountantContentModuleAssembly(),
            ImageViewerAssembly()
        ])
    }

}

