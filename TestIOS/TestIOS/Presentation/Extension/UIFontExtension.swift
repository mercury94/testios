//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit

extension UIFont {

    class func appFontSemibold(withSize size: Float) -> UIFont? {
        return UIFont.systemFont(ofSize: CGFloat(size), weight: UIFontWeightSemibold)
    }

    class func appFontMedium(withSize size: Float) -> UIFont? {
        return UIFont.systemFont(ofSize: CGFloat(size), weight: UIFontWeightMedium)
    }

    class func appFontRegular(withSize size: Float) -> UIFont? {
        return UIFont.systemFont(ofSize: CGFloat(size), weight: UIFontWeightRegular)
    }

    class func appFontUltraLight(withSize size: Float) -> UIFont? {
        return UIFont.systemFont(ofSize: CGFloat(size), weight: UIFontWeightUltraLight)
    }
}
