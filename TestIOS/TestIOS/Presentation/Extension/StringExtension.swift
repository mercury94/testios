//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

extension String {

    public static func isNullOrEmpty(string: String?) -> Bool {
        return string?.isEmpty ?? true
    }

}