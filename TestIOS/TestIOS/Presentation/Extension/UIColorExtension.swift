//
// Created by Nikita on 01.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit


extension UIColor {

    static func color(fromRed red: Float, green: Float, blue: Float) -> UIColor {
        return color(fromRed: red, green: green, blue: blue, alpha: 1.0)
    }

    static func color(fromRed red: Float, green: Float, blue: Float, alpha: Float) -> UIColor {
        return UIColor(red: CGFloat(red / 255.0), green: CGFloat(green / 255.0), blue: CGFloat(blue / 255.0), alpha: CGFloat(alpha / 255.0))
    }

    static var defaultApplicationColor: UIColor {
        return color(fromRed: 0.0, green: 0.0, blue: 0.0)
    }
    
}
