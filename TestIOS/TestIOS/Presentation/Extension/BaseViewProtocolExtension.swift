//
// Created by Nikita on 06.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation

extension BaseViewProtocol {

    //Переопределение данного метода, чтобы принимать параметры по умолчанию
    func showAlertDialogQuestionWithTitle(_ title: String,
                                          message: String,
                                          onClickPositiveButtonCallback: (() -> (Void))? = nil,
                                          onClickNegativeButtonCallback: (() -> (Void))? = nil) {
        return showAlertDialogQuestionWithTitle(title,
                message: message,
                onClickPositiveButtonCallback: onClickPositiveButtonCallback,
                onClickNegativeButtonCallback: onClickNegativeButtonCallback)
    }
}
