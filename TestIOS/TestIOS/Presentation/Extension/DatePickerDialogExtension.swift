//
// Created by Nikita on 06.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import DatePickerDialog

extension DatePickerDialog {

    func showDefaultTimePickerDialog(_ title: String, callback: @escaping DatePickerDialog.DatePickerCallback) {
        show(title, doneButtonTitle: R.string.localizable.done(),
                cancelButtonTitle: R.string.localizable.cancel(),
                datePickerMode: .time,
                callback: callback)
    }

}
