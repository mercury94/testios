//
// Created by Nikita on 29.09.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//
// Базовые протоколы для взаимодействия View и Presentera


import Foundation

protocol BasePresenterProtocol {

    var baseView: BaseViewProtocol? { get set }

    func viewDidLoad(view: BaseViewProtocol)

    func viewDidAppear(view: BaseViewProtocol)

    func viewDidDisappear(view: BaseViewProtocol)

}

protocol BaseViewProtocol {

    var basePresenter: BasePresenterProtocol? { get set }

    func initViews()

    func setTitle(title: String)

    func showMessage(message: String)

    func showPending()

    func hidePending()

    func showAlertDialogQuestionWithTitle(_ title: String,
                                          message: String,
                                          onClickPositiveButtonCallback: (() -> (Void))?,
                                          onClickNegativeButtonCallback: (() -> (Void))?)

}
