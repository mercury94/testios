//
// Created by Nikita on 10.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

class BasePageViewController : UIPageViewController, BaseViewProtocol {

    var basePresenter: BasePresenterProtocol? = nil
    var progressIndicator: UIActivityIndicatorView?

    //region BaseViewProtocol

    func initViews() {
        setupProgressIndicator()
        navigationItem.leftBarButtonItem = createLeftBarButtonItem()
        navigationItem.rightBarButtonItem = createRightBarButtonItem()
    }

    func setupProgressIndicator() {
        progressIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        progressIndicator?.frame = CGRect(x: 0, y: 0, width: 40, height: 40);
        progressIndicator?.center = self.view.center
        if progressIndicator != nil {
            self.view.addSubview(progressIndicator!)
        }
        progressIndicator?.bringSubview(toFront: self.view)
    }

    func setTitle(title: String) {
#if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) : setTitle.title = \(title)")
#endif
    }

    func showMessage(message: String) {

#if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) : showMessage.text = \(message)")
#endif
        self.view.makeToast(message, duration: 3.0, position: .bottom)
    }

    func showPending() {
#if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) : showPending")
#endif
        progressIndicator?.startAnimating()
    }

    func hidePending() {
#if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) : hidePending")
#endif
        progressIndicator?.stopAnimating()
    }


    func createLeftBarButtonItem() -> UIBarButtonItem? {
        return nil
    }


    func createRightBarButtonItem() -> UIBarButtonItem? {
        return nil
    }


    func showAlertDialogQuestionWithTitle(_ title: String,
                                          message: String,
                                          onClickPositiveButtonCallback: (() -> (Void))? = nil,
                                          onClickNegativeButtonCallback: (() -> (Void))? = nil) {

        let alert = UIAlertController(title: title,
                message: message,
                preferredStyle: .alert)

        let onClickNegativeAction = UIAlertAction(title: R.string.localizable.no(),
                style: .default
        ) { _ in
            onClickNegativeButtonCallback?()
        }

        alert.addAction(onClickNegativeAction)

        let onClickPositiveAction = UIAlertAction(title: R.string.localizable.yes(),
                style: .default
        ) { _ in
            onClickPositiveButtonCallback?()
        }

        alert.addAction(onClickPositiveAction)

        navigationController?.present(alert, animated: true, completion: nil)

    }

    //endregion BaseViewProtocol

    //region lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
#if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) : viewDidLoad")
#endif
        basePresenter?.viewDidLoad(view: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
#if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) : viewDidAppear")
#endif
        basePresenter?.viewDidAppear(view: self)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
#if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) : viewDidDisappear")
#endif
        basePresenter?.viewDidDisappear(view: self)
    }

    //endregion lifecycle

}
