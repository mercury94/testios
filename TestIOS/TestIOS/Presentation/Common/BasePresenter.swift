//
// Created by Nikita on 29.09.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation

class BasePresenter: BasePresenterProtocol {
    var baseView: BaseViewProtocol? = nil

    func viewDidLoad(view: BaseViewProtocol) {
        view.initViews()
    }

    func viewDidAppear(view: BaseViewProtocol) {
        baseView?.setTitle(title: R.string.localizable.appName())
    }

    func viewDidDisappear(view: BaseViewProtocol) {
        view.hidePending()
    }

    func viewDidUnload() {
        //override
    }
}
