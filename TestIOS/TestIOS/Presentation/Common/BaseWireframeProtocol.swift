//
// Created by Nikita on 29.09.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import UIKit
import Swinject

protocol BaseWireframeProtocol {

    var container: Container? { get set }
    var viewController: UIViewController? { get set }
}
