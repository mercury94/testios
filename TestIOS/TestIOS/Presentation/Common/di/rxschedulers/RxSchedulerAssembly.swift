//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift
import Swinject

class RxSchedulerAssembly: Assembly {

    public static let mainScheduler = "main_scheduler"
    public static let serialScheduler: String = "serial_scheduler"

    public let main = mainScheduler
    public let serial = serialScheduler

    func assemble(container: Container) {

        container.register(ImmediateSchedulerType.self, name: main) { _ in
            MainScheduler.instance
        }

        container.register(ImmediateSchedulerType.self, name: serial) { _ in
            SerialDispatchQueueScheduler(qos: .default)
        }
    }
}
