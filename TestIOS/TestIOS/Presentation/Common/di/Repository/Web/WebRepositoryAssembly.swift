//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class WebRepositoryAssembly: Assembly {
    func assemble(container: Container) {

        container.register(QuoteWebRepositoryProtocol.self){ _ in
            QuoteWebRepository()
        }

    }
}
