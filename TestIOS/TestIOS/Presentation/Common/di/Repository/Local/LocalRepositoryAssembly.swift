//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class LocalRepositoryAssembly: Assembly {

    func assemble(container: Container) {

        container.register(AccountantLocalRepositoryProtocol.self) { _ in
            AccountantLocalRepository()
        }

        container.register(LeaderLocalRepositoryProtocol.self) { _ in
            LeaderLocalRepository()
        }

        container.register(WorkerLocalRepositoryProtocol.self) { _ in
            WorkerLocalRepository()
        }

    }
}
