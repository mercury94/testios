//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Swinject

class RepositoryAssembly: Assembly {

    func assemble(container: Container) {

        container.register(AccountantRepositoryProtocol.self) { r in
            return AccountantRepository(localRepository: r.resolve(AccountantLocalRepositoryProtocol.self)!)
        }

        container.register(LeaderRepositoryProtocol.self) { r in
            return LeaderRepository(localRepository: r.resolve(LeaderLocalRepositoryProtocol.self)!)
        }

        container.register(WorkerRepositoryProtocol.self) { r in
            return WorkerRepository(localRepository: r.resolve(WorkerLocalRepositoryProtocol.self)!)
        }

        container.register(QuoteRepositoryProtocol.self) { r in
            return QuoteRepository(webRepository: r.resolve(QuoteWebRepositoryProtocol.self)!)
        }

    }
}
