//
// Created by Nikita on 02.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import UIKit

class TabBarItemViewController: BaseViewController {
    override func initViews() {
        super.initViews()
        parent?.navigationItem.leftBarButtonItems = nil
        parent?.navigationItem.rightBarButtonItems = nil
        parent?.navigationItem.leftBarButtonItem = createLeftBarButtonItem()
        parent?.navigationItem.rightBarButtonItem = createRightBarButtonItem()
    }
}
