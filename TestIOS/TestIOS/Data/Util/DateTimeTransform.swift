//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import ObjectMapper

class DateTimeTransform: TransformType {
    public typealias Object = Date
    public typealias JSON = Double
    let dateFormatter = DateFormatter()

    public init() {
        dateFormatter.dateFormat = "yyyy-dd-MM HH:mm:ss"
    }

    open func transformFromJSON(_ value: Any?) -> Date? {


        if let timeStr = value as? String {
            return dateFormatter.date(from: timeStr)
        }

        return nil
    }

    open func transformToJSON(_ value: Date?) -> Double? {
        fatalError("not implementing")
        return nil
    }
}
