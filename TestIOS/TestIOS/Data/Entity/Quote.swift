//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import ObjectMapper

class Quote: Mappable {

    var id: CLong? = 0
    var description: String? = nil
    var time: String? = nil
    var rating: Int? = 0

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        id <- map["id"]
        description <- map["description"]
        time <- map["time"]
        rating <- map["rating"]
    }
}

