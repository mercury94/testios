//
// Created by Nikita on 02.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation

class Worker: Employee {

    @objc dynamic var numberWorkplace: String?

    @objc dynamic var lunchBeginTime: String?

    @objc dynamic var lunchEndTime: String?

}
