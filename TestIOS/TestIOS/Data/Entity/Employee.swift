//
// Created by Nikita on 02.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RealmSwift
import Foundation

class Employee: Object {

    @objc dynamic var id = UUID().uuidString
    @objc dynamic var name: String?
    @objc dynamic var secondName: String?
    @objc dynamic var patronymic: String?
    @objc dynamic var salary: String?
    @objc dynamic var position = 0

    override static func primaryKey() -> String? {
        return "id"
    }

}
