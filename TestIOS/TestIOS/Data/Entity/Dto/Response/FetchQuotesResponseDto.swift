//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import ObjectMapper

class FetchQuotesResponseDto: Mappable {

    var total: CLong = 0
    var last: Date? = nil
    var quotes: Array<Quote>? = nil

    required init?(map: Map) {

    }

    func mapping(map: Map) {
        total <- map["total"]
        last <- (map["last"], DateTimeTransform())
        quotes <- map["quotes"]
    }
}
