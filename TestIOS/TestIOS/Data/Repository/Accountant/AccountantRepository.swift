//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class AccountantRepository: AccountantRepositoryProtocol {

    private let localRepository: AccountantLocalRepositoryProtocol

    init(localRepository: AccountantLocalRepositoryProtocol) {
        self.localRepository = localRepository
    }

    func save(employee: Accountant) -> Observable<Bool> {
        return localRepository.save(employee: employee)
    }

    func save(employees: [Accountant]) -> Observable<Bool> {
        return localRepository.save(employees: employees)
    }

    func remove(employee: Accountant) -> Observable<Bool> {
        return localRepository.remove(employee: employee)
    }

    func fetchAll() -> Observable<Array<Accountant>> {
        return localRepository.fetchAll()
    }
}
