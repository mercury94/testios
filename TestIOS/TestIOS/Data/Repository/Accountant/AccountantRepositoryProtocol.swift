//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

protocol AccountantRepositoryProtocol {

    func save(employee: Accountant) -> Observable<Bool>

    func save(employees: [Accountant]) -> Observable<Bool>

    func remove(employee: Accountant) -> Observable<Bool>

    func fetchAll() -> Observable<Array<Accountant>>

}
