//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift
import RealmSwift

class AccountantLocalRepository: AccountantLocalRepositoryProtocol {

    func save(employee: Accountant) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                observer.on(.next(try self.createOrUpdate(realm: realm, accountant: employee)))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func save(employees: [Accountant]) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                var result = true
                try employees.forEach { accountant in
                    let r = try self.createOrUpdate(realm: realm, accountant: accountant)
                    result = result && r
                }
                observer.on(.next(result))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func remove(employee: Accountant) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                try realm.write {
                    realm.delete(realm.object(ofType: Accountant.self, forPrimaryKey: employee.id)!)
                    observer.on(.next(true))
                }
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func fetchAll() -> Observable<Array<Accountant>> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                let result = realm.objects(Accountant.self).sorted(byKeyPath: "position")
                observer.on(.next(Array(result).map { a -> Accountant in
                    Accountant(value: a)
                }))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    private func createOrUpdate(realm: Realm, accountant: Accountant) throws -> Bool {
        try realm.write {
            let rl = realm.object(ofType: Accountant.self, forPrimaryKey: accountant.id)
            if (rl == nil) {
                let maxPosition = realm.objects(Accountant.self).max(ofProperty: "position") as Int?
                accountant.position = maxPosition == nil ? 1 : maxPosition! + 1
                realm.add(accountant)
            } else {
                realm.add(accountant, update: true)
            }
        }
        return true
    }
}
