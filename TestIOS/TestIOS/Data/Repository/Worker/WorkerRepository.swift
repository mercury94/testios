//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class WorkerRepository: WorkerRepositoryProtocol {

    private let localRepository: WorkerLocalRepositoryProtocol

    init(localRepository: WorkerLocalRepositoryProtocol) {
        self.localRepository = localRepository
    }

    func save(employee: Worker) -> Observable<Bool> {
        return localRepository.save(employee: employee)
    }

    func save(employees: [Worker]) -> Observable<Bool> {
        return localRepository.save(employees: employees)
    }

    func remove(employee: Worker) -> Observable<Bool> {
        return localRepository.remove(employee: employee)
    }

    func fetchAll() -> Observable<Array<Worker>> {
        return localRepository.fetchAll()
    }
}
