//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift
import RealmSwift

class WorkerLocalRepository: WorkerLocalRepositoryProtocol {

    func save(employee: Worker) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                observer.on(.next(try self.createOrUpdate(realm: realm, worker: employee)))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func save(employees: [Worker]) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                var result = true
                try employees.forEach { worker in
                    let r = try self.createOrUpdate(realm: realm, worker: worker)
                    result = result && r
                }
                observer.on(.next(result))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func remove(employee: Worker) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                try realm.write {
                    realm.delete(realm.object(ofType: Worker.self, forPrimaryKey: employee.id)!)
                    observer.on(.next(true))
                }
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func fetchAll() -> Observable<Array<Worker>> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                let result = realm.objects(Worker.self).sorted(byKeyPath: "position")
                observer.on(.next(Array(result).map { w -> Worker in
                    Worker(value: w)
                }))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }

            return Disposables.create()
        }
    }


    private func createOrUpdate(realm: Realm, worker: Worker) throws -> Bool {
        try realm.write {
            let rw = realm.object(ofType: Worker.self, forPrimaryKey: worker.id)
            if (rw == nil) {
                let maxPosition = realm.objects(Worker.self).max(ofProperty: "position") as Int?
                worker.position = maxPosition == nil ? 1 : maxPosition! + 1
                realm.add(worker)
            } else {
                realm.add(worker, update: true)
            }
        }
        return true
    }
}
