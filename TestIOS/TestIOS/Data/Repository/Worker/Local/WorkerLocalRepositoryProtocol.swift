//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

protocol WorkerLocalRepositoryProtocol {

    func save(employee: Worker) -> Observable<Bool>

    func save(employees: [Worker]) -> Observable<Bool>

    func remove(employee: Worker) -> Observable<Bool>

    func fetchAll() -> Observable<Array<Worker>>
}
