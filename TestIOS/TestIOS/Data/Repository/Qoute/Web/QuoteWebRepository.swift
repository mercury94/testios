//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift
import Alamofire
import AlamofireObjectMapper

class QuoteWebRepository: QuoteWebRepositoryProtocol {

    private let fetchQuotesUrl = "http://quotes.zennex.ru/api/v3/bash/quotes?sort=time"
    
    func fetchQuotes() -> Observable<Array<Quote>> {
        return Observable.create { observer in
            Alamofire.request(self.fetchQuotesUrl).responseObject { (response: DataResponse<FetchQuotesResponseDto>) in
                let responseDto = response.result.value
                observer.on(.next(responseDto != nil ? responseDto!.quotes! : Array()))
                observer.on(.completed)
            }

            return Disposables.create()
        }


    }
}
