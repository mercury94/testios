//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

protocol QuoteWebRepositoryProtocol {

    func fetchQuotes() -> Observable<Array<Quote>>

}
