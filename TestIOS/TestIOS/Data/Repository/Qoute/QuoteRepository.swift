//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class QuoteRepository: QuoteRepositoryProtocol {

    private let webRepository: QuoteWebRepositoryProtocol

    init(webRepository: QuoteWebRepositoryProtocol) {
        self.webRepository = webRepository
    }

    func fetchQuotes() -> RxSwift.Observable<Array<Quote>> {
        return webRepository.fetchQuotes()
    }
}
