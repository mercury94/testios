//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift
import RealmSwift

class LeaderLocalRepository: LeaderLocalRepositoryProtocol {

    func save(employee: Leader) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                observer.on(.next(try self.createOrUpdate(realm: realm, leader: employee)))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func save(employees: [Leader]) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                var result = true
                try employees.forEach { leader in
                    let r = try self.createOrUpdate(realm: realm, leader: leader)
                    result = result && r
                }
                observer.on(.next(result))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func remove(employee: Leader) -> Observable<Bool> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                try realm.write {
                    realm.delete(realm.object(ofType: Leader.self, forPrimaryKey: employee.id)!)
                    observer.on(.next(true))
                }
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }

    func fetchAll() -> Observable<Array<Leader>> {
        return Observable.create { observer in
            do {
                let realm = try Realm()
                let result = realm.objects(Leader.self).sorted(byKeyPath: "position")
                observer.on(.next(Array(result).map { l -> Leader in
                    Leader(value: l)
                }))
                observer.on(.completed)
            } catch let error as NSError {
                observer.on(.error(error))
            }
            return Disposables.create()
        }
    }


    private func createOrUpdate(realm: Realm, leader: Leader) throws -> Bool {
        try realm.write {
            let rl = realm.object(ofType: Leader.self, forPrimaryKey: leader.id)
            if (rl == nil) {
                let maxPosition = realm.objects(Leader.self).max(ofProperty: "position") as Int?
                leader.position = maxPosition == nil ? 1 : maxPosition! + 1
                realm.add(leader)
            } else {
                realm.add(leader, update: true)
            }
        }
        return true
    }
}
