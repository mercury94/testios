//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class LeaderRepository: LeaderRepositoryProtocol {


    private let localRepository: LeaderLocalRepositoryProtocol

    init(localRepository: LeaderLocalRepositoryProtocol) {
        self.localRepository = localRepository
    }

    func save(employee: Leader) -> Observable<Bool> {
        return localRepository.save(employee: employee)
    }

    func save(employees: [Leader]) -> Observable<Bool> {
        return localRepository.save(employees: employees)
    }

    func remove(employee: Leader) -> Observable<Bool> {
        return localRepository.remove(employee: employee)
    }

    func fetchAll() -> Observable<Array<Leader>> {
        return localRepository.fetchAll()
    }
}
