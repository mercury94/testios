//
// Created by Nikita on 04.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

protocol LeaderRepositoryProtocol {

    func save(employee: Leader) -> Observable<Bool>

    func remove(employee: Leader) -> Observable<Bool>

    func fetchAll() -> Observable<Array<Leader>>

    func save(employees: [Leader]) -> Observable<Bool>


}
