//
// Created by Nikita on 08.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift
import SwiftSoup

class FetchQuotesInteractor: BaseInteractor<Void, Array<Quote>> {

    private var quoteRepository: QuoteRepositoryProtocol?

    init(quoteRepository: QuoteRepositoryProtocol,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)
        self.quoteRepository = quoteRepository
    }

    override func buildObservable(param: Void?) -> RxSwift.Observable<Array<Quote>> {
        return quoteRepository!.fetchQuotes()
                .map { array -> Array<Quote> in
                    return array.map { quote -> Quote in
                        do {
                            let doc: Document = try SwiftSoup.parse(quote.description!)
                            quote.description = try doc.text()
                        } catch Exception.Error(let type, let message) {
                            print("Error parse quote : \(message)")
                        } catch {
                            print("Error parse quote: message = nil")
                        }
                        return quote
                    }
                }
    }
}
