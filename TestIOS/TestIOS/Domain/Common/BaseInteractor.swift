//
// Created by Nikita on 02.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

struct ComposeTransformer<T, R> {
    let transformer: (Observable<T>) -> Observable<R>

    init(transformer: @escaping (Observable<T>) -> Observable<R>) {
        self.transformer = transformer
    }

    func call(_ observable: Observable<T>) -> Observable<R> {
        return transformer(observable)
    }
}

extension ObservableType {
    func compose<T>(_ transformer: ComposeTransformer<E, T>) -> Observable<T> {
        return transformer.call(self.asObservable())
    }
}

class BaseInteractor<Param, Result> {

    let workerScheduler: ImmediateSchedulerType!
    let observerScheduler: ImmediateSchedulerType!
    var compositeDisposable: CompositeDisposable!

    internal var observable: Observable<Result>?

    init(workerScheduler: ImmediateSchedulerType!, observerScheduler: ImmediateSchedulerType!) {
        self.workerScheduler = workerScheduler
        self.observerScheduler = observerScheduler
        self.compositeDisposable = CompositeDisposable()
    }

    public func execute(onSubscribe: (() -> ())? = nil,
                        onNext: ((Result) -> Void)? = nil,
                        onError: ((Swift.Error) -> Void)? = nil,
                        onCompleted: (() -> Void)? = nil,
                        onDisposed: (() -> Void)? = nil,
                        param: Param? = nil) {

        observable = buildObservable(param: param)
                .compose(applySchedulers())
                .do(onDispose: { self.observable = nil })
                .do(onSubscribe: onSubscribe)

        addDisposable(disposable: observable!.subscribe(onNext: onNext, onError: onError, onCompleted: onCompleted, onDisposed: onDisposed))
    }

    func buildObservable(param: Param? = nil) -> Observable<Result> {
        return Optional.none!
    }

    private func addDisposable(disposable: Disposable!) {
        if compositeDisposable.isDisposed {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable.insert(disposable)
    }

    public func dispose() {
        if compositeDisposable.isDisposed {
            compositeDisposable.dispose()
        }
    }

    func applySchedulers() -> ComposeTransformer<Result, Result> {
        return ComposeTransformer<Result, Result> { observable in
            return observable
                    .subscribeOn(self.workerScheduler)
                    .observeOn(self.observerScheduler)
        }
    }


}
