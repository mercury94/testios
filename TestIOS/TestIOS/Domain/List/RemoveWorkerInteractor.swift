//
// Created by Nikita on 06.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class RemoveWorkerInteractor: BaseInteractor<Worker, Bool> {

    private var workerRepository: WorkerRepositoryProtocol?

    init(workerRepository: WorkerRepositoryProtocol!,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)
        self.workerRepository = workerRepository
    }

    override func buildObservable(param: Worker?) -> RxSwift.Observable<Bool> {
        return workerRepository!.remove(employee: param!)
    }

}
