//
// Created by Nikita on 07.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class SortEmployeesInteractor: BaseInteractor<(accountants: Array<Accountant>,
                                               leaders: Array<Leader>,
                                               workers: Array<Worker>), (accountants: Array<Accountant>,
                                                                         leaders: Array<Leader>,
                                                                         workers: Array<Worker>)> {

    var saveWorkerInteractor: SaveWorkerInteractor?
    var saveLeaderInteractor: SaveLeaderInteractor?
    var saveAccountantInteractor: SaveAccountantInteractor?
    var fetchEmployeeListInteractor: FetchListEmployeeInteractor?

    init(saveWorkerInteractor: SaveWorkerInteractor,
         saveLeaderInteractor: SaveLeaderInteractor,
         saveAccountantInteractor: SaveAccountantInteractor,
         fetchEmployeeListInteractor: FetchListEmployeeInteractor,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)

        self.saveAccountantInteractor = saveAccountantInteractor
        self.saveWorkerInteractor = saveWorkerInteractor
        self.saveLeaderInteractor = saveLeaderInteractor
        self.fetchEmployeeListInteractor = fetchEmployeeListInteractor
    }

    override func buildObservable(param: (accountants: Array<Accountant>,
                                          leaders: Array<Leader>,
                                          workers: Array<Worker>)?) -> Observable<(accountants: Array<Accountant>,
                                                                                   leaders: Array<Leader>,
                                                                                   workers: Array<Worker>)> {
        return Observable.create { observer in

            let leaders = self.sortLeaders(leaders: param!.leaders)
            let workers = self.sortWorkers(workers: param!.workers)
            let accountants = self.sortAccountants(accountants: param!.accountants)
           
                    observer.on(.next((accountants, leaders, workers)))
                    observer.onCompleted()
                    return Disposables.create()
                }
                .flatMap { (e: (accountants: Array<Accountant>,
                                leaders: Array<Leader>,
                                workers: Array<Worker>)) -> Observable<(Bool, Bool, Bool)> in
                    //Сохраняем измененые списки
                    return Observable.zip(self.saveAccountantInteractor!.buildObservable(param: e.accountants),
                            self.saveWorkerInteractor!.buildObservable(param: e.workers),
                            self.saveLeaderInteractor!.buildObservable(param: e.leaders)) { e, e1, e2 in
                        return (e, e1, e2)
                    }
                }
                .flatMap { _ -> Observable<(accountants: Array<Accountant>,
                                            leaders: Array<Leader>,
                                            workers: Array<Worker>)> in
                    //Возвращаем, что сохранилось
                    return self.fetchEmployeeListInteractor!.buildObservable(param: nil)
                }
    }
    
    private func sortLeaders(leaders: [Leader]) -> [Leader] {
        
        //Сортировка руководитлей
        let result = leaders.sorted { (leader: Leader, leader1: Leader) -> Bool in
            "\(leader.secondName!)\(leader.name!)\(leader.patronymic!)" < "\(leader1.secondName!)\(leader1.name!)\(leader1.patronymic!)"
        }
        
        //Обновление позиций у руководитлей
        for (index, element) in result.enumerated() {
            element.position = index + 1
        }
        return result
    }
    
    private func sortWorkers(workers: [Worker]) -> [Worker] {
        
        //Сортировка Сотрудников
        let result = workers.sorted { (worker: Worker, worker1: Worker) -> Bool in
            "\(worker.secondName!)\(worker.name!)\(worker.patronymic!)" < "\(worker1.secondName!)\(worker1.name!)\(worker1.patronymic!)"
        }
        
        //Обновление позиций у сотрудников
        
        for (index, element) in result.enumerated() {
            element.position = index + 1
        }
        
        return result
    }

    private func sortAccountants(accountants: [Accountant]) -> [Accountant]{
        
        //Сортировка бухгалтеров
        let result = accountants.sorted { (accountant: Accountant, accountant1: Accountant) -> Bool in
            "\(accountant.secondName!)\(accountant.name!)\(accountant.patronymic!)" < "\(accountant1.secondName!)\(accountant1.name!)\(accountant1.patronymic!)"
        }
        //Обновление позиций у бухгалтеров
        for (index, element) in result.enumerated() {
            element.position = index + 1
        }
        return result
    }


}
