//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class SaveWorkerInteractor: BaseInteractor<[Worker], Bool> {

    private var workerRepository: WorkerRepositoryProtocol?

    init(workerRepository: WorkerRepositoryProtocol!,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)
        self.workerRepository = workerRepository
    }

    override func buildObservable(param: [Worker]?) -> Observable<Bool> {
        return param?.count == 1 ?
                workerRepository!.save(employee: param!.first!) :
                workerRepository!.save(employees: param!)
    }

}
