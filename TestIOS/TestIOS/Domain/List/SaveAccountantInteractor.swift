//
// Created by Nikita on 05.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class SaveAccountantInteractor: BaseInteractor<[Accountant], Bool> {

    private var accountantRepository: AccountantRepositoryProtocol?

    init(accountantRepository: AccountantRepositoryProtocol,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)
        self.accountantRepository = accountantRepository
    }

    override func buildObservable(param: [Accountant]?) -> Observable<Bool> {
        return param?.count == 1 ?
                accountantRepository!.save(employee: param!.first!) :
                accountantRepository!.save(employees: param!)
    }
}
