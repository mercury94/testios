//
// Created by Nikita on 06.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class RemoveLeaderInteractor : BaseInteractor<Leader, Bool>{

    private var leaderRepository: LeaderRepositoryProtocol?

    init(leaderRepository: LeaderRepositoryProtocol,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)
        self.leaderRepository = leaderRepository
    }

    override func buildObservable(param: Leader?) -> Observable<Bool> {
        return leaderRepository!.remove(employee: param!)
    }

}
