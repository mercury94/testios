//
// Created by Nikita on 03.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import Foundation
import RxSwift

class FetchListEmployeeInteractor: BaseInteractor<Void, (accountants: Array<Accountant>,
                                                         leaders: Array<Leader>,
                                                         workers: Array<Worker>)> {

    private var accountantRepository: AccountantRepositoryProtocol?
    private var leaderRepository: LeaderRepositoryProtocol?
    private var workerRepository: WorkerRepositoryProtocol?

    init(accountantRepository: AccountantRepositoryProtocol,
         leaderRepository: LeaderRepositoryProtocol,
         workerRepository: WorkerRepositoryProtocol?,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)
        self.accountantRepository = accountantRepository
        self.leaderRepository = leaderRepository
        self.workerRepository = workerRepository
    }

    override func buildObservable(param: Void?) -> Observable<(accountants: Array<Accountant>,
                                                               leaders: Array<Leader>,
                                                               workers: Array<Worker>)> {
        return Observable.zip(getObservableFetchAccountants(),
                getObservableFetchLeaders(),
                getObservableFetchWorker()) { e, e1, e2 in
            return (e, e1, e2)
        }
    }

    private func getObservableFetchAccountants() -> Observable<Array<Accountant>> {
        return accountantRepository!.fetchAll().subscribeOn(workerScheduler)
    }

    private func getObservableFetchLeaders() -> Observable<Array<Leader>> {
        return leaderRepository!.fetchAll().subscribeOn(workerScheduler)
    }

    private func getObservableFetchWorker() -> Observable<Array<Worker>> {
        return workerRepository!.fetchAll().subscribeOn(workerScheduler)
    }

}
