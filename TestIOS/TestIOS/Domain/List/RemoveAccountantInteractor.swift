//
// Created by Nikita on 06.10.17.
// Copyright (c) 2017 Nikita. All rights reserved.
//

import RxSwift

class RemoveAccountantInteractor: BaseInteractor<Accountant, Bool> {

    private var accountantRepository: AccountantRepositoryProtocol?

    init(accountantRepository: AccountantRepositoryProtocol,
         workerScheduler: ImmediateSchedulerType!,
         observerScheduler: ImmediateSchedulerType!) {
        super.init(workerScheduler: workerScheduler, observerScheduler: observerScheduler)
        self.accountantRepository = accountantRepository
    }


    override func buildObservable(param: Accountant?) -> RxSwift.Observable<Bool> {
        return accountantRepository!.remove(employee: param!)
    }
}
